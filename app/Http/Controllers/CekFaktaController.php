<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class CekFaktaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/cekfakta');
        // }
        $item = Http::get('https://api.solopos.com/api/breaking/posts?category=670832');
        $breaking = $item->json();

        $xmlPath = Config::get('xmldata.breaking');
        $xmlPath2 = Config::get('xmldata.topic');
        $story = Helper::read_xml($xmlPath, 'breaking-story');
        $cekfakta = $breaking; //Helper::read_xml($xmlPath, 'breaking-cekfakta');
        $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $kolom = Helper::read_xml($xmlPath, 'breaking-kolom');
        $news = Helper::read_xml($xmlPath, 'breaking-news');
        $lifestyle = Helper::read_xml($xmlPath, 'breaking-lifestyle');
        //$widget = Helper::read_xml($xmlPath2, 'Ekspedisi-Ekonomi-Digital-2021');
        $datawidget = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=781384');
        $widget = $datawidget->json();


        $header = [
            'title' => 'Cek Fakta',
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://www.solopos.com/cekfakta',
            'category' => 'Cek Fakta',
            'category_parent' => 'Cek Fakta',
            'is_premium' => '',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        ];

        return view('pages.archive', ['story' => $story, 'data' => $cekfakta, 'header' => $header, 'popular' => $popular, 'news' => $news, 'lifestyle' => $lifestyle, 'kolom' => $kolom , 'widget' => $widget]);
    }
}
