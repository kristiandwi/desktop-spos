<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class TerpopulerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/terpopuler');
        // }

        $xmlPath = Config::get('xmldata.breaking');

        // $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $res = Http::get('https://tf.solopos.com/api/v1/stats/popular/all');
        $data = $res->json();
        $popular = $data['data'];      
        $story = Helper::read_xml($xmlPath, 'breaking-story');

        $header = array(
            'title' => 'Berita Terpopuler Terkini Hari Ini',
            'category' => 'Terpopuler',
            'category_parent' => 'Terpopuler',
            'is_premium' => '',
            'focusKeyword' => 'Berita Terpopuler',
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://www.solopos.com/terpopular',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        );

        return view('pages.popular', ['popular' => $popular, 'story' => $story, 'header' => $header]);
    }
}
