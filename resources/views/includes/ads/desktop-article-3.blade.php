<div class="iklan mt-3 mb-3" align="center">
@if( date('Y-m-d H:i:s') >= '2022-03-08 00:00:01' && date('Y-m-d H:i:s') <= '2022-07-01 23:59:59')
	<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
	<script>
	window.googletag = window.googletag || {cmd: []};
	googletag.cmd.push(function() {
		googletag.defineSlot('/54058497/Desktop-Artikel-3', [[640, 200], [640, 150], [640, 100]], 'div-gpt-ad-1658136320717-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});
	</script>
	<!-- /54058497/Desktop-Artikel-3 -->
	<div id='div-gpt-ad-1658136320717-0' style='min-width: 640px; min-height: 100px;'>
	<script>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1658136320717-0'); });
	</script>
	</div>
@else
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<ins class="adsbygoogle"
		style="display:block; text-align:center;"
		data-ad-layout="in-article"
		data-ad-format="fluid"
		data-ad-client="ca-pub-4969077794908710"
		data-ad-slot="6460499125"></ins>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
@endif
</div>

						