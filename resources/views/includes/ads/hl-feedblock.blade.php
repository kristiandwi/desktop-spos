@if( date('Y-m-d H:i:s') >= '2022-10-01 06:00:01' && date('Y-m-d H:i:s') <= '2022-10-01 23:59:59')

<div class="item post-overaly-style post-md" style="background-image:url(https://images.solopos.com/2022/09/IMG_4489.JPG.jpg)">
	<div class="featured-post">
		<a class="image-link" href="https://www.solopos.com/sasar-konsumen-yang-batasi-gula-yakult-light-diluncurkan-1435878?utm_source=terkini_desktop" title="Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan">&nbsp;</a>
		<div class="overlay-post-content">
			<div class="post-content">
				<div class="grid-category">
					<a class="post-cat bisnis" href="{{ url("/bisnis") }}">Bisnis</a>
				</div>

				<h2 class="post-title title-md">
					<a href="https://www.solopos.com/sasar-konsumen-yang-batasi-gula-yakult-light-diluncurkan-1435878?utm_source=terkini_desktop" title="Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan">Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan</a>
				</h2>
				<div class="post-meta">
					<ul>
						<li><a href="#"><i class="fa fa-user"></i> Mariyana Ricky P.D</a></li>
						<li><a href="#"><i class="icon icon-clock"></i> 01 Oktober 2022</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!--/ Featured post end -->
</div><!-- Item 1 end -->

@endif