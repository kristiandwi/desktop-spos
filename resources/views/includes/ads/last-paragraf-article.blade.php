@if( date('Y-m-d H:i:s') >= '2021-11-27 00:00:01' && date('Y-m-d H:i:s') <= '2021-11-28 23:59:59')
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<ins class="adsbygoogle"
		style="display:block; text-align:center;"
			data-ad-layout="in-article"
			data-ad-format="fluid"
			data-ad-client="ca-pub-4969077794908710"
			data-ad-slot="6460499125"></ins>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
@else

<!-- PubMatic ad tag (Javascript) : solopos.com_desktop_320x250 | TADEX_Web-PT_Aksara_Solopos- | 300 x 250 Sidekick -->
<script type="text/javascript">
	var pubId=157566;
	var siteId=841452;
	var kadId=3732071;
	var kadwidth=300;
	var kadheight=250;
	var kadschain="SUPPLYCHAIN_GOES_HERE";
	var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
	var kadtype=1;
	var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
	var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
	var kadexpdir = '1,2,3,4,5';
	var kadbattr = '8,9,10,11,14';
	var kadifb = 'Dc';
	var kadpageurl= "%%PATTERN:url%%";
</script>
<script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>

@endif