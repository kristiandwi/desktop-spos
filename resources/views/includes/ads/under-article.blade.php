<div style="margin: 20px 0;">
@if( date('Y-m-d H:i:s') >= '2022-03-08 00:00:01' && date('Y-m-d H:i:s') <= '2022-05-06 23:59:59')
	<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
	<script>
	window.googletag = window.googletag || {cmd: []};
	googletag.cmd.push(function() {
		googletag.defineSlot('/54058497/desktop_under_articel', [[640, 100], [640, 180], [640, 200], [640, 150]], 'div-gpt-ad-1644561262632-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});
	</script>
	<!-- /54058497/desktop_under_articel -->
	<div id='div-gpt-ad-1644561262632-0' style='min-width: 640px; min-height: 100px;'>
	<script>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1644561262632-0'); });
	</script>
	</div>
@elseif( date('Y-m-d H:i:s') >= '2022-03-05 00:00:01' && date('Y-m-d H:i:s') <= '2022-10-05 23:59:59') 
	<iframe width="100%" height="362px" data-src="https://www.youtube.com/embed/8TAUUsNSWak?rel=0&amp;autoplay=1&mute=1" title="Presiden & Tokoh: Solopos Harus Jadi Panduan Informasi Terpercaya | 25 Tahun Solopos Media Group" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
	<script>
		function init() {
		var vidDefer = document.getElementsByTagName('iframe');
		for (var i=0; i<vidDefer.length; i++) {
		if(vidDefer[i].getAttribute('data-src')) {
		vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
		} } }
		window.onload = init;		
	</script>
@else
	<!-- ads under article -->
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<ins class="adsbygoogle" 
			style="display:block; text-align:center;"
			data-ad-layout="in-article"
			data-ad-format="fluid"
			data-ad-client="ca-pub-4969077794908710"
			data-ad-slot="6460499125"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
@endif

@if( date('Y-m-d H:i:s') >= '2022-03-05 00:00:01' && date('Y-m-d H:i:s') <= '2022-07-07 23:59:59') 
<video width="100%" autoplay loop controls muted>
	<source src="https://cdn.solopos.com/iklan/DSC2022/Dsc22_Short.mp4" type="video/mp4">
	Your browser does not support the video.
</video>
@endif
</div>
						