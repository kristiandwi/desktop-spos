@if( date('Y-m-d H:i:s') >= '2022-10-01 00:00:01' && date('Y-m-d H:i:s') <= '2022-10-24 23:59:59' && Cookie::get('is_login') == null && Cookie::get('is_membership') == null || Cookie::get('is_membership') == 'free')
<div id="mega-billboard-container" class="smooth" data-height="200px">
	<div id="div-big" class="smooth">
		<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/Desktop-Wrapt-Top', [[996, 100], [996, 200]], 'div-gpt-ad-1636519332484-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		</script>
		<!-- /54058497/Desktop-Wrapt-Top -->
		<div id='div-gpt-ad-1636519332484-0' style='min-width: 996px; min-height: 100px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1636519332484-0'); });
			</script>
		</div>
	</div>
</div>
@else
{{--<div id="mega-billboard-container" class="smooth" data-height="200px">
	<div id="div-big" class="smooth">
		<a href="https://id.solopos.com/login" target="_blank"><img src="https://images.solopos.com/plus/esposvaganza-996.gif" title="Espos Plus"></a>
	</div>
</div>--}}
@endif