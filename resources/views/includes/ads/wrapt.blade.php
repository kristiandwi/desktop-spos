@if( date('Y-m-d H:i:s') >= '2022-10-01 00:00:01' && date('Y-m-d H:i:s') <= '2022-10-24 23:59:59' && Cookie::get('is_login') == null && Cookie::get('is_membership') == null || Cookie::get('is_membership') == 'free')
<div id="skinad-left">            
	<div id="left-lk">
		<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/Desktop-Wrapt-Left', [250, 750], 'div-gpt-ad-1636519405633-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		</script>
		<!-- /54058497/Desktop-Wrapt-Left -->
		<div id='div-gpt-ad-1636519405633-0' style='min-width: 250px; min-height: 750px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1636519405633-0'); });
			</script>
		</div>
	</div>
</div>

<div id="skinad-right">
	<div id="right-lk">
		<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		  window.googletag = window.googletag || {cmd: []};
		  googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/Desktop-Wrapt-Right', [250, 750], 'div-gpt-ad-1636519455141-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		  });
		</script>
		<!-- /54058497/Desktop-Wrapt-Right -->
		<div id='div-gpt-ad-1636519455141-0' style='min-width: 250px; min-height: 750px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1636519455141-0'); });
			</script>
		</div>
	</div>
</div>

@endif