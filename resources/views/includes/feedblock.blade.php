@if( date('Y-m-d H:i:s') >= '2022-05-02 12:00:01' && date('Y-m-d H:i:s') <= '2022-10-08 23:59:59')

<div class="col-12 mb-10">
	<div class="post-block-style">
		<div class="row">
			<div class="col-md-5">
				<div class="post-thumb post-list_feed">
					<img src="https://images.solopos.com/2022/09/IMG_4489.JPG.jpg" alt="Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
					<a class="post-cat-box bisnis" href=""> Bisnis</a>
				</div>
			</div>
			<div class="col-md-7 pl-0">
				<div class="post-content">
					<h2 class="post-title title-md">
						<a href="https://www.solopos.com/sasar-konsumen-yang-batasi-gula-yakult-light-diluncurkan-1435878" title="Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan">Sasar Konsumen yang Batasi Gula, Yakult Light Diluncurkan</a>
						</h2>											
					<div class="post-meta mb-7">
						<span class="post-author"><a href="#"><i class="fa fa-user"></i> Mariyana Ricky P.D</a></span>
						<span class="post-date"><i class="fa fa-clock-o"></i>  01 Oktober 2022</span>
					</div>
					<p>PT Yakult Indonesia Persada meluncurkan produk baru, Yakult Light yang merupakan varian dari Yakult Original dengan kandungan gula lebih sedikit.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endif

@if( date('Y-m-d H:i:s') >= '2022-04-17 16:00:00' && date('Y-m-d H:i:s') <= '2022-09-28 23:59:59')

<div class="col-12 mb-10">
	<div class="post-block-style">
		<div class="row">
			<div class="col-md-5">
				<div class="post-thumb post-list_feed">
					<img src="https://images.solopos.com/2022/09/sewa-bus-pariwisata-TRAC.jpg" alt="Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
					<a class="post-cat-box bisnis" href=""> Otomotif</a>
				</div>
			</div>
			<div class="col-md-7 pl-0">
				<div class="post-content">
					<h2 class="post-title title-md">
						<a href="https://www.solopos.com/tips-sewa-bus-pariwisata-biar-liburan-gak-gagal-total-1426180" title="Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total">Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total</a>
						</h2>											
					<div class="post-meta mb-7">
						<span class="post-author"><a href="#"><i class="fa fa-user"></i> BC</a></span>
						<span class="post-date"><i class="fa fa-clock-o"></i>  20 September 2022</span>
					</div>
					<p>Bus pariwisata sewa harus dipastikan sesuai dengan kebutuhan Anda agar tak salah pilih.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endif

@if( date('Y-m-d') == '2022-04-04' or date('Y-m-d') == '2022-04-10' or date('Y-m-d') == '2022-04-17' or date('Y-m-d') == '2022-04-25')

<div class="col-12 mb-10">
	<div class="post-block-style">
		<div class="row">
			<div class="col-md-5">
				<div class="post-thumb post-list_feed">
					<img src="https://images.solopos.com/2022/04/bc-dynamix.jpg" alt="Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
					<a class="post-cat-box bisnis" href=""> Bisnis</a>
				</div>
			</div>
			<div class="col-md-7 pl-0">
				<div class="post-content">
					<h2 class="post-title title-md">
						<a href="https://www.solopos.com/dynamix-hadirkan-3-pilihan-produk-semen-sesuai-kebutuhan-1287758" title="Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan">Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan</a>
						</h2>											
					<div class="post-meta mb-7">
						<span class="post-author"><a href="#"><i class="fa fa-user"></i> BC</a></span>
						<span class="post-date"><i class="fa fa-clock-o"></i>  03 April 2022</span>
					</div>
					<p>Ada tiga produk semen Dynamix yang memberikan kualitas bangunan bermutu dengan harga terjangkau.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endif

@if( date('Y-m-d') <= '2022-04-11')

<div class="col-12 mb-10">
	<div class="post-block-style">
		<div class="row">
			<div class="col-md-5">
				<div class="post-thumb post-list_feed">
					<img src="https://images.solopos.com/2022/04/bus.jpg" alt="Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
					<a class="post-cat-box otomotif" href=""> Otomotif</a>
				</div>
			</div>
			<div class="col-md-7 pl-0">
				<div class="post-content">
					<h2 class="post-title title-md">
						<a href="https://www.solopos.com/sewa-bus-kini-bisa-online-lebih-mudah-dan-banyak-pilihan-1288106" title="Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!">Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!</a>
						</h2>											
					<div class="post-meta mb-7">
						<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
						<span class="post-date"><i class="fa fa-clock-o"></i>  04 April 2022</span>
					</div>
					<p>Pelayanan sewa bus kini semakin mudah dengan pelayanan online melalui aplikasi TRACtoGO.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endif