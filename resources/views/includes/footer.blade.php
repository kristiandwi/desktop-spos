	<!-- newsletter start -->
	<!--div class="newsletter-area">
		<div class="container">
			<div class="row ts-gutter-30 justify-content-center align-items-center">
				<div class="col-lg-7 col-md-6">
					<div class="footer-loto">
						<a href="#">
							<img src="https://www.solopos.com/assets/tema/mobile/img/logo-putih.png" width=150 alt="">
						</a>
					</div>
				</div>

				<div class="col-lg-5 col-md-6">
					<div class="footer-newsletter">
						<form action="#" method="post">
							<div class="email-form-group">
								<i class="news-icon fa fa-paper-plane" aria-hidden="true"></i>
								<input type="email" name="EMAIL" class="newsletter-email" placeholder="Your email" required>
								<input type="submit" class="newsletter-submit" value="Subscribe">
							</div>

						</form>
					</div>
				</div>

			</div>

		</div>

	</div-->
	<!-- newsletter end-->

	<!-- Footer start -->
	<div class="ts-footer">
		<div class="container">
			<div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
				<div class="col-lg-7 col-md-7">
					<div class="footer-widget">
						<div class="footer-logo">
							<img loading="lazy" src="{{ url('images/logo.png') }}" alt="Footer Logo">
						</div>
						<div class="widget-content">
							<ul class="ts-footer-nav">
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/kontak') }}">KONTAK</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/copyright') }}">COPYRIGHT</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/jagad-jawa') }}">JAGAD JAWA</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/about-us') }}">REDAKSI</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/about-us') }}">TENTANG KAMI</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/videos') }}">VIDEO</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/loker') }}">KARIR</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/kode-etik') }}">KODE ETIK</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/cekfakta') }}">CEK FAKTA</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/privacy-policy') }}">PRIVACY POLICY</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/page/code-of-conduct') }}">PEDOMAN MEDIA SIBER</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="{{ url('/terpopuler') }}">TERPOPULER</a></li>

							</ul>
						</div>
					</div>
				</div><!-- col end -->
				<div class="col-lg-5 col-md-5">
					<div class="footer-widtet post-widget">
						<h3 class="widget-title"><span>Jaringan</span></h3>
						<div class="widget-content">

							<ul class="ts-footer-nav">
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.bisnis.com" title="Bisnis.com" target="_blank" rel="noopener noreferrer">BISNIS.COM</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.harianjogja.com" title="Harianjogja.com" target="_blank" rel="noopener noreferrer">HARIANJOGJA.COM</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://bisnismuda.id" title="Bisnismuda.id" target="_blank" rel="noopener noreferrer">BISNISMUDA.ID</a></li>
								{{--<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.iklan.solopos.com" title="Iklan Solopos" target="_blank" rel="noopener noreferrer">IKLAN</a></li>--}}
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.tokosolopos.com" title="Tokosolopos.com" target="_blank" rel="noopener noreferrer">TOKO</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.semarangpos.com" title="Semarangpos.com" target="_blank" rel="noopener noreferrer">SEMARANGPOS.COM</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.griya190.com" title="Griya190.com" target="_blank" rel="noopener noreferrer">GRIYA190.COM</a></li>
								{{-- <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.jeda.id" title="Jeda.id" target="_blank" rel="noopener noreferrer">JEDA.ID</a></li> --}}
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://pisalin.com/" title="Pisalin" target="_blank" rel="noopener noreferrer">PISALIN</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.madiunpos.com" title="Madiunpos.com" target="_blank" rel="noopener noreferrer">MADIUNPOS.COM</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.rumah190.com" title="Rumah190.com" target="_blank" rel="noopener noreferrer">RUMAH190.COM</a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="" title="" target="_blank" rel="noopener noreferrer"></a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="" title="" target="_blank" rel="noopener noreferrer"></a></li>
								<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.ibukotakita.com" title="Ibukotakita.com" target="_blank" rel="noopener noreferrer">IBUKOTAKITA.COM</a></li>
							</ul>

						</div>
					</div>
				</div><!-- col end -->
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- Footer End-->

	<!-- ts-copyright start -->
	<div class="ts-copyright">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-12 text-center">
					<div class="copyright">
						<p>Copyright &copy; 2007-<?php echo date("Y");?>, Solopos Digital Media - Panduan Informasi & Inspirasi. All rights reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ts-copyright end-->

	<!-- backto -->
	<div class="top-up-btn">
		<div class="backto" style="display: block;">
			<a href="#" class="icon icon-arrow-up" aria-hidden="true"></a>
		</div>
	</div>
	<!-- backto end-->

	<!-- Javascript Files
	================================================== -->

	<!-- Popper Jquery -->
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<!-- Bootstrap jQuery -->
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<!-- magnific-popup -->
	<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
	<!-- Owl Carousel -->
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<!-- Color box -->
	<script src="{{ asset('js/jquery.colorbox.js') }}"></script>
	<!-- Template custom -->
	<script src="{{ asset('js/custom.js') }}"></script>

	<script id="dsq-count-scr" src="//solopos.disqus.com/count.js" async></script>

	@stack('custom-scripts')

	<script>
	function showHide(d)
	{
	var onediv = document.getElementById(d);
	var divs=['div-small','div-big'];
	for (var i=0;i<divs.length;i++)
	  {
	  if (onediv != document.getElementById(divs[i]))
	    {
	    document.getElementById(divs[i]).style.display='none';
	    }
	  }
	onediv.style.display = 'block';
	}

	</script>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9K2TV3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- ads frame left-right -->

    {{-- <a href="https://www.alexa.com/siteinfo/solopos.com"><script type='text/javascript' src='https://xslt.alexa.com/site_stats/js/s/a?url=solopos.com'></script></a> --}}
    {{--
	<!-- Histats.com  START  (aync)-->
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,4327171,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4327171&101" alt="web statistics" border="0"></a></noscript>
    <!-- Histats.com  END -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--}}
