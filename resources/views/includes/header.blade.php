<head>
    <script type="text/javascript">
        if (screen.width <= 640) {
            var pathname = window.location.pathname;
            window.location.href = "https://m.solopos.com"+pathname;
    }
    </script>
    @if(isset($header['category']) || isset($header['category_parent']))
        @if ($header['category'] == 'Sekolah' || $header['category_parent'] == 'Sekolah')
            <script type="text/javascript">
                var pathname = window.location.pathname;
                if(pathname === '/sekolah') {
                    window.location.href = "https://sekolah.solopos.com";
                } else {
                    window.location.href = "https://sekolah.solopos.com"+pathname;
                }
            </script>
        @endif
    @endif
    
    <title>{{ $header['title'] }} - Solopos.com | Panduan Informasi dan Inspirasi</title>
    <meta content="{{ $header['title'] }}" itemprop="headline" />
    <meta name="title" content="{{ $header['title'] }}" />
    <meta name="description" content="{{ $header['description'] }}" itemprop="description" />
    <meta name="keywords" content="{{ $header['news_keyword'] }}" itemprop="keywords" />
    <meta name="news_keywords" content="{{ $header['news_keyword'] }}" itemprop="keywords" />
    <meta name="author" content="{{ $header['author'] }}"  itemprop="author"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1200, initial-scale=1">
    <meta name="copyright" content="Solopos Media Group" />
    <meta name="googlebot-news" content="index,follow" />
    <meta name="googlebot" content="index,follow,all" />
    <meta name="Bingbot" content="index,follow">
    <meta name="robots" content="index,follow,max-image-preview:large" />
    <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
    <meta name="yandex-verification" content="7966bb082003f8ae" />
    <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
    <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="showus-verification" content="pub-2627" />

    @if(isset($header['is_single']) == 'yes')
    <!-- Primary Meta Tags -->
    <meta property="article:section" content="{{ $header['category'] }}">
    <meta property="article:publisher" content="https://www.facebook.com/soloposcom" />
    <meta property="article:author" content="{{ $header['author'] }}" />
    <meta property="article:published_time" content="{{ $header['publish_time'] }}" />
    <meta name="pubdate" content="{{ $header['publish_time'] }}" itemprop="datePublished" />
    <meta content="{{ $header['publish_time'] }}" itemprop="dateCreated" />
    <meta content="{{ $header['link'] }}" itemprop="url" />
    <link rel="image_src" href="{{ $header['image'] }}" />
    <link rel="canonical" href="{{ $header['link'] }}" />
    <!-- Open Graph / Facebook -->
    <meta property="fb:app_id" content="1438927622798153"/>
    <meta property="fb:pages" content="395076396062" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $header['title'] }}" />
    <meta property="og:description" content="{{ $header['description'] }}" />
    <meta property="og:image" content="{{ $header['image'] }}" />
    <meta property="og:url" content="{{ $header['link'] }}" />
    <meta property="og:image:width" content="{{ $header['img_width'] }}" />
    <meta property="og:image:height" content="{{ $header['img_height'] }}" />
    <meta property="og:site_name" content="Solopos.com" />
    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{ $header['title'] }}" />
    <meta name="twitter:description" content="{{ $header['description'] }}" />
    <meta name="twitter:image" content="{{ $header['image'] }}" />
    <meta name="twitter:image:src" content="{{ $header['image'] }}" />
    <meta name="twitter:site" content="@soloposdotcom" />

    <meta property="dable:item_id" content="{{ $header['id'] }}">
    <meta property="dable:title" content="{{ $header['title'] }}">
    <meta property="dable:image" content="{{ $header['image'] }}">
    <meta property="dable:author" content="{{ $header['author'] }}">
    <meta property="article:section" content="solopos.com">
    <meta property="article:section2" content="{{ $header['category'] }}">
    <meta property="article:section3" content="{{ $header['category_child'] }}">
    <meta property="article:published_time" content="{{ $header['publish_time'] }}">

    <script type="application/ld+json">
    {
        "@context":"http://schema.org",
        "@type":"NewsArticle",
        "headline":"{{ $header['title'] }}",
        "datePublished":"{{ $header['publish_time'] }}",
        "dateModified":"{{ $header['publish_time'] }}",
        "mainEntityOfPage":{
            "@type":"WebPage",
            "@id":"{{ $header['link'] }}"
        },
        "description":"{{ $header['description'] }}",
        "image":{
            "@type":"ImageObject",
            "url":"{{ $header['image'] }}",
            "width":{{ $header['img_width'] }},
            "height":{{ $header['img_height'] }}
        },
        "author":{
            "@type":"Person",
            "name":"{{ $header['author'] }}",
            "url":"https://www.solopos.com/penulis/{{ $author_slug[0] }}"
        },
        "editor":{
          "@type":"Person",
          "name":"{{ $header['editor'] }}",
          "url":"https://www.solopos.com/author/{{ $header['editor_url'] }}"
        },
        "publisher":{
            "@type":"Organization",
            "name":"Solopos.com",
            "url" : "https://www.solopos.com/",
            "sameAs" : [
                "https://www.facebook.com/soloposcom/",
                "https://twitter.com/soloposdotcom",
                "https://www.instagram.com/koransolopos/"
            ],
            "logo":{
                "@type":"ImageObject",
                "url":"https://www.solopos.com/images/logo.png",
                "width":360,
                "height":63
            }
        }
    }
    </script>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebPage",
      "headline": "{{ $header['title'] }}",
      "url": "{{ $header['link'] }}",
      "datePublished": "{{ $header['publish_time'] }}",
      "image": "{{ $header['image'] }}",
      "thumbnailUrl" : "{{ $header['image'] }}"
    }
    </script>

    <script type="application/ld+json">
    {
        "@context":"http://schema.org",
        "@type":"BreadcrumbList",
        "itemListElement":[{
            "@type":"ListItem",
            "position":1,"name":"{{ $header['category_parent'] }}",
            "item":"https://www.solopos.com/{{ $content['category_parent'] }}"
        }@if($content['category_child']!='')

        ,
        {
            "@type":"ListItem",
            "position":2,
            "name":"{{ $header['category_child'] }}",
            "item":"https://www.solopos.com/{{ $content['category_parent'] }}/{{ $content['category_child'] }}"
        }@endif]
    }
    </script>
    <link rel="amphtml" href="{{ $header['link'] }}/amp" data-component-name="amp:html:link">
    @else
    <!-- Open Graph / Facebook -->
    <meta property="fb:app_id" content="1438927622798153"/>
    <meta property="fb:pages" content="395076396062" />
    <meta property="article:publisher" content="https://www.facebook.com/soloposcom" />
    <meta property="og:title" content="{{ $header['title'] }}" />
    <meta property="og:description" content="{{ $header['description'] }}" />
    <meta property="og:url" content="{{ $header['link'] }}" />
    <meta property="og:image" content="{{ $header['image'] }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="750">
    <meta property="og:image:height" content="500">
    <meta property="og:site_name" content="Solopos.com" />
    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{ $header['title'] }}" />
    <meta name="twitter:description" content="{{ $header['description'] }}" />
    <meta name="twitter:image" content="{{ $header['image'] }}" />
    <meta name="twitter:image:src" content="{{ $header['image'] }}" />
    <meta name="twitter:site" content="@soloposdotcom" />
    <meta http-equiv="refresh" content="900">
    <script type='application/ld+json'>
    {
        "@context" : "https://schema.org",
        "@type" : "Organization",
        "name" : "Solopos.com",
        "url" : "https://www.solopos.com/",
        "sameAs" : [
            "https://www.facebook.com/soloposcom/",
            "https://twitter.com/soloposdotcom",
            "https://www.instagram.com/koransolopos/"
        ],
        "logo": "{{ url('images/xlogo.png.pagespeed.ic.r-renCjxN7.webp') }}"
    }
    </script>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "url": "https://www.solopos.com/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.solopos.com/search?s={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>
    @endif
    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-7051417-1', 'auto', {'useAmpClientId': true});
      ga('set', 'dimension1', '{{ $header['editor'] }}');
      ga('set', 'dimension2', '{{ $header['category'] ?? 'News' }}{{ $header['is_premium'] }}');
      ga('set', 'dimension3', '{{ $header['keyword'] }}');
      ga('set', 'dimension4', '{{ $header['focusKeyword'] ?? 'Berita Terkini' }}');
      ga('set', 'dimension5', '{{ $header['publish_time'] ?? '' }}');
      ga('set', 'dimension6', '{{ $header['news_keyword'] ?? '' }}');
      ga('set', 'dimension7', '{{ $header['author'] }}');
      ga('set', 'dimension8', 'solopos.com');
      ga('send', 'pageview');
    </script>

{{--   <!-- Matomo -->
<script>
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://appx.solopos.com/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
--}}

    <link rel="image_src" href="{{ $header['image'] }}" />
    <link rel="canonical" href="{{ $header['link'] }}" />

	<!--Favicon-->
	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
	<!-- preload -->
    <link rel="preload" href="{{ asset('css/style.css')}}?v={{time()}}" as="style">
    <link rel="preload" href="{{ asset('css/bootstrap.min.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/iconfonts.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/owl.carousel.min.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/owl.theme.default.min.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/magnific-popup.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/animate.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/responsive.css') }}" as="style">
    <link rel="preload" href="{{ asset('css/colorbox.css') }}" as="style">
	<link rel="preload" href="{{ asset('js/jquery.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/popper.min.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/bootstrap.min.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/jquery.magnific-popup.min.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/owl.carousel.min.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/jquery.colorbox.js') }}" as="script">
	<link rel="preload" href="{{ asset('js/custom.js') }}" as="script">

    <!-- Stylesheet -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/iconfonts.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />
	{{-- <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}?v={{time()}}">
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
	<link rel="stylesheet" href="{{ asset('css/colorbox.css') }}">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <!-- initialize jQuery Library -->
	<script src="{{ asset('js/jquery.js') }}"></script>
    @include('includes.custom-script')
    {{--<script type="application/javascript">document.interestCohort = null;</script>--}}
    @stack('tracking-scripts')
</head>
