<div class="col-lg-4 template-sidebar">
    <div class="widget widget-border">
      <div class="widget-heading">
        <h3 class="title">Search</h3>
      </div>
      <div class="widget-search">
        <form>
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" placeholder="Search">
            <span class="input-group-addon">
          <button type="submit">
              <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                      d="M6.41471 12.8294C7.83796 12.8291 9.22019 12.3527 10.3413 11.4759L13.8662 15.0008L15 13.867L11.4751 10.3421C12.3523 9.22088 12.8291 7.83833 12.8294 6.41471C12.8294 2.8778 9.95162 0 6.41471 0C2.8778 0 0 2.8778 0 6.41471C0 9.95162 2.8778 12.8294 6.41471 12.8294ZM6.41471 1.60368C9.068 1.60368 11.2257 3.76143 11.2257 6.41471C11.2257 9.068 9.068 11.2257 6.41471 11.2257C3.76143 11.2257 1.60368 9.068 1.60368 6.41471C1.60368 3.76143 3.76143 1.60368 6.41471 1.60368Z"
                      fill="#041A57" />
              </svg>
          </button>
      </span>
          </div>
        </form>
      </div>
    </div>
    {{--<div class="widget">
      <div class="widget-banner">
        <img src="image/banner/ad-banner1.jpg" alt="Banner">
        <div class="content-box">
          <h2 class="title"><span>2021</span>TALK ABOUT YOUR PROJECT.</h2>
        </div>
      </div>
    </div>--}}
    <div class="widget widget-border">
      <div class="widget-heading">
        <h3 class="title">Terpopular Solopos.com</h3>
      </div>
      <div class="widget-recent-post">
      @php
          $loop_no = 1;
      @endphp
      @foreach ($popular as $pop)
      @php           
      //$thumb = $postsf['featured_image']['thumbnail'] ?? 'https://www.solopos.com/images/no-thumb.jpg'; 
      //$medium = $postsf['featured_image']['medium'] ?? 'https://www.solopos.com/images/no-medium.jpg';

      $thumb = $pop['images']['url_thumb'] ?? 'https://www.solopos.com/images/no-thumb.jpg'; 
      $medium = $pop['images']['thumbnail'] ?? 'https://www.solopos.com/images/no-medium.jpg';
      $title = html_entity_decode($pop['title']);
      @endphp
      @if($loop_no<=5) 
      <div class="single-post">
          <div class="figure-box">
            <a href="{{ url("/{$pop['slug']}-{$pop['id']}") }}" class="link-item"><img src="{{ $thumb }}" alt="{{ $title }}"  width="100px"></a>
          </div>
          <div class="content-box">
            <h3 class="entry-title" style="overflow: hidden; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;"><a href="{{ url("/{$pop['slug']}-{$pop['id']}") }}">{{ $title }}</a></h3>
            <div class="entry-date">{{ Carbon\Carbon::parse($pop['date'])->translatedFormat('j M Y') }}</div>
          </div>
        </div>
      @endif
      @php $loop_no++; @endphp
      @endforeach

        {{--<div class="single-post">
          <div class="figure-box">
            <a href="{{ url('/solorayakeren') }}" class="link-item"><img src="image/blog/post2.jpg" alt="Post"></a>
          </div>
          <div class="content-box">
            <h3 class="entry-title"><a href="{{ url('/solorayakeren') }}">UX and UI Design at Trends 2020</a></h3>
            <div class="entry-date">April 15, 2021</div>
          </div>
        </div>
        <div class="single-post">
          <div class="figure-box">
            <a href="{{ url('/solorayakeren') }}" class="link-item"><img src="image/blog/post3.jpg" alt="Post"></a>
          </div>
          <div class="content-box">
            <h3 class="entry-title"><a href="{{ url('/solorayakeren') }}">UX and UI Design to Trends 2020</a></h3>
            <div class="entry-date">April 13, 2021</div>
          </div>
        </div>--}}
      </div>
    </div>
    <div class="widget widget-border">
      <div class="widget-heading">
        <h3 class="title">Follow Us</h3>
      </div>
      <div class="widget-follow-us">
        <ul class="social">
          <li>
            <a target="_blank" href="https://www.facebook.com/soloposcom" class="facebook"><i
              class="fab fa-facebook-f"></i></a>
          </li>
          <li>
            <a target="_blank" href="https://twitter.com/soloposdotcom" class="twitter"><i
              class="fab fa-twitter"></i></a>
          </li>
          <li>
            <a target="_blank" href="https://www.linkedin.com/solopos" class="linkedin"><i
              class="fab fa-linkedin-in"></i></a>
          </li>
          <li>
            <a target="_blank" href="https://www.instagram.com/koransolopos" class="instagram"><i
              class="fab fa-instagram"></i></a>
          </li>
          <li>
            <a target="_blank" href="https://www.pinterest.com/soloposcom" class="pinterest"><i
              class="fab fa-pinterest-p"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <div class="widget widget-border">
      <div class="widget-heading">
        <h3 class="title">Tag</h3>
      </div>
      <div class="widget-tag">
        <div class="tagcloud">
          <a href="#" class="tag-cloud-link">OJK</a>
          <a href="#" class="tag-cloud-link">OVO</a>
          <a href="#" class="tag-cloud-link">BNI 46</a>
          <a href="#" class="tag-cloud-link">Shipper</a>
          <a href="#" class="tag-cloud-link">Candi Elektronik</a>
          <a href="#" class="tag-cloud-link">Telkom Indonesia</a>
          <a href="#" class="tag-cloud-link">Blesscon</a>
          <a href="#" class="tag-cloud-link">JNE Solo</a>
          <a href="#" class="tag-cloud-link">Alila Solo</a>
        </div>
      </div>
    </div>
  </div>