	<!-- Header start -->
	<body id="">
    <div id="mega-billboard-container" class="smooth" data-height="400px">

        <div id="div-big" class="smooth">
            <!--<a target="_blank" href="https://rs-jih.co.id/rsjihsolo/medsos">
                <img src="{{ url('/images/bugar/wrapt-atas.gif') }}" width="996px">
            </a>-->
			<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
			<script>
			window.googletag = window.googletag || {cmd: []};
			googletag.cmd.push(function() {
				googletag.defineSlot('/54058497/RSJIH-WRAP-TOP', [996, 200], 'div-gpt-ad-1640664297629-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
			</script>
			<!-- /54058497/RSJIH-WRAP-TOP -->
			<div id='div-gpt-ad-1640664297629-0' style='min-width: 996px; min-height: 200px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640664297629-0'); });
			</script>
			</div>
        </div>
    </div>

    <div id="skinad-left">            
        <div id="left-lk">
		{{--<a href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
                <img src="https://cdn.solopos.com/iklan/JIH_KIRI.gif" width="250px" height="750px">
            </a>--}}
			<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
			<script>
			window.googletag = window.googletag || {cmd: []};
			googletag.cmd.push(function() {
				googletag.defineSlot('/54058497/RSJIH-WRAP-LEFT', [250, 750], 'div-gpt-ad-1640664331662-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
			</script>
			<!-- /54058497/RSJIH-WRAP-LEFT -->
			<div id='div-gpt-ad-1640664331662-0' style='min-width: 250px; min-height: 750px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640664331662-0'); });
			</script>
			</div>
        </div>
    </div>

    <div id="skinad-right">
        <div id="right-lk">
            <a href="https://www.instagram.com/rs.jihsolo/" target="_blank">
                <img src="https://cdn.solopos.com/banner/bugar/wrapt-kanan.gif" width="250px" height="750px">
            </a>
			{{--<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
			<script>
			window.googletag = window.googletag || {cmd: []};
			googletag.cmd.push(function() {
				googletag.defineSlot('/54058497/RSJIH-WRAPT-RIGHHT', [250, 750], 'div-gpt-ad-1640664369202-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
			</script>
			<!-- /54058497/RSJIH-WRAPT-RIGHT -->
			<div id='div-gpt-ad-1640664369202-0' style='min-width: 250px; min-height: 750px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640664369202-0'); });
			</script>
			</div>--}}
        </div>
    </div>
	
	<header id="header" class="header">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-md-3 col-sm-12">
					<div class="logo">
						 <a href="{{ url('/rsjihsolo') }}">
							<img src="https://cdn.solopos.com/banner/bugar/logo-bugar.png" alt="Logo" style="width:200px;">
						 </a>
					</div>
				</div><!-- logo col end -->

				<div class="col-md-8 col-sm-12 p-0 header-right">
					<div class="ad-banner float-right">

					</div>
				</div><!-- header right end -->
			</div><!-- Row end -->
		</div><!-- Logo and banner area end -->
	</header><!--/ Header end -->

	<div class="main-nav clearfix is-ts-sticky">
		<div class="container">
			<div class="row justify-content-between">
				<nav class="navbar navbar-expand-lg col-lg-0">
					<div class="site-nav-inner float-left">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
						<span class="fa fa-bars"></span>
					</button>
					   <!-- End of Navbar toggler -->
					   <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav">
								<li class="sticky">
									<img src="https://cdn.solopos.com/banner/bugar/logo-bugar-st.png" height="22px" width="100px" alt="Logo">
								</li>
								<li>
									<a href="{{ url('/rsjihsolo') }}" style="font-size: 14px; padding-right: 25px;">Beranda</a>
								</li>
								<li>
									<a href="{{ url('/rsjihsolo/artikel') }}" style="font-size: 14px; padding-right: 25px;">Artikel</a>
								</li>
								<li>
									<a href="{{ url('/rsjihsolo/foto') }}" style="font-size: 14px; padding-right: 25px;">Galeri Foto</a>
								</li>																
								<li>
									<a href="{{ url('/rsjihsolo/grafis') }}" style="font-size: 14px; padding-right: 25px;">Infografis</a>
								</li>
								<li>
									<a href="{{ url('/rsjihsolo/video') }}" style="font-size: 14px; padding-right: 25px;">Galeri Video</a>
								</li>
								<li>
									<a href="{{ url('/rsjihsolo/promo') }}" style="font-size: 14px; padding-right: 25px;">Promo</a>
								</li>
								<li>
									<a href="{{ url('/rsjihsolo/kontak') }}" style="font-size: 14px; padding-right: 25px;">Kontak Kami</a>
								</li>
				
							</ul><!--/ Nav ul end -->
						</div><!--/ Collapse end -->

					</div><!-- Site Navbar inner end -->
				</nav><!--/ Navigation end -->

				<div class="col-lg-3 pr-0 text-right nav-social-wrap">
					<a href="https://www.solopos.com">
					<img src="https://cdn.solopos.com/banner/bugar/logo-solopos.png"></a>

					<div class="nav-search">
						<a href="#search-popup" class="xs-modal-popup">
							<i class="icon icon-search1"></i>
						</a>
					</div><!-- Search end -->
						
					<div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="xs-search-panel">
									<form class="ts-search-group">
										<div class="input-group">
											<input type="search" class="form-control" name="s" placeholder="Search" value="">
											<button class="input-group-btn search-button">
												<i class="icon icon-search1"></i>
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- End xs modal -->
				</div>
			</div><!--/ Row end -->
		</div><!--/ Container end -->	

		<div class="trending-bar trending-light d-md-block">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-9 text-center text-md-left">
						<p class="trending-title"><i class="tsicon fa fa-bolt"></i> STORY </p>
						<div id="trending-slide" class="owl-carousel owl-theme trending-slide">
							@php $sto_loop = 1; @endphp
							@foreach($story as $sto) @if($sto_loop <= 5)
							<div class="item">
							   <div class="post-content">
							      <h2 class="post-title title-small">
							         <a href="{{ url("/{$sto['slug']}-{$sto['id']}") }}" target="_blank" title="{{ $sto['title'] }}">{{ $sto['title'] }}</a>
							      </h2>
							   </div>
							</div>
					      	@endif
							@php $sto_loop++; @endphp
							@endforeach
						</div>
					</div><!-- Col end -->
					<div class="col-md-3 text-md-right text-center">
						<div class="ts-date">
							<i class="fa fa-calendar-check-o"></i>{{ date("j F Y") }}
						</div>
					</div><!-- Col end -->
				</div><!--/ Row end -->
			</div><!--/ Container end -->
		</div><!--/ Trending end -->			
	</div><!-- Menu wrapper end -->
	
    <!-- ads floating -->

	<div class="gap-50"></div>
	
    
