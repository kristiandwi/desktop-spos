		<!-- Widget Solo Great Sale -->
        <a href="https://www.sologreatsale.com/?utm_source=widget_solopos.com" title="Solo Great Sale" target="_blank" style="display: inline-block; width: 100%;">
            <img src="{{ url('images/banner/solo-great-sale.png') }}" class="visible animated" width="100%" alt="solo great sale">
        </a>
        <div class="energi-widget">
            <ul>
            @php $eu_loop = 1; @endphp
            @foreach ($widget as $item)
            @if($eu_loop <=10) 
            <li> 
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=widget_solopos.com" style="color: rgb(143 3 57);"> 
                    <img src="{{ $item['images']['thumbnail'] }}" style="object-fit: none; object-position: center; height: 60px; width: 80px;" alt="{{ $item['title'] }}"> 
                    {{ $item['title'] }}
                </a> 
                <div style="clear:both;"></div>
            </li>
            @endif
            @php $eu_loop++ @endphp
            @endforeach 
            </ul>                                   
        </div>
        <div class="mb-3"></div>
        <div class="energi-copy">
            <a href="https://www.sologreatsale.com/?utm_source=widget_solopos.com" title="Solo Great Sale" target="_blank">
                <img src="{{ url('images/banner/solo-great-sale.png') }}" class="visible animated" width="100%" alt="solo great sale">
            </a>
        </div>
    
    
          <style type="text/css">
            .energi-widget ul {
                list-style: none;
                margin: 0;
                padding: 0;
                max-height: 270px;
                overflow-y: scroll;
                overflow-x: hidden;        
            }
            .energi-widget ul li {
                list-style: none;
                /*margin-bottom: 10px;*/
                display: block;
                color: blue;
                font-weight: bold;
                font-family: arial;
                padding: 10px;
                line-height: 17px;
            }
            .energi-widget ul li:last-child {
                border: none;
            }
            .energi-widget ul li a {
                text-decoration: none;
                color: #1EBAC4;
                font-size: 12px;
            }
            .energi-widget ul li a:hover {
                text-decoration: none;
                color: #FFA500;     
            }
            .energi-widget ul li img {
                width: 80px;
                height: 60px;
                float: left;
                margin-right: 10px;
                vertical-align: center;
            }
            .energi-widget-title {
                height: 60px;
                background: #FFA500;
            }
            .energi-widget-title img {
                height: 50px;
                padding: 5px 20px;
                float: left;
            }
            .energi-copy {
            border-top: none;
            }     
          </style>