@extends('layouts.app-amp')
@section('content')
<div class="full-post">
  <div class="news-box-content">
      <h1 class="news-box-content-title">
        {{ $header['title'] }}
      </h1>
      <p class="news-box-content-sub"><em>{{ $header['ringkasan'] }}</em></p>
      <p class="news-box-content-sub">
        <em>{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB</em>
        
        @if($header['author'] != $header['editor'] )
        
        <em class="multi-reporter" style="margin-top:10px;margin-bottom:-7px;">
          <em style="display:inline-block;">Penulis:</em>
        @foreach ($author_slug as $author)
        <a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }}/amp @else {{ url('/') }}/penulis/{{$author}}/amp @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
        @endforeach
        </em>
        @endif
       
        <em style="display:inline-block;">Editor:</em>  <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }}/amp @else https://www.solopos.com/arsip @endif"  target="_blank">{{ $header['editor'] }}</a> <em style="display:inline-block;">| Solopos.com</em>
      </p>
  </div>    
  @if(!empty($video))
  <amp-youtube data-videoid="{{ $video }}" layout="responsive" width="480" height="270"></amp-youtube>
  @else
  <amp-img src="{{ $content['image'] }}" alt="{{ htmlspecialchars_decode($content['title']) }}" layout="responsive" width="600" height="400"></amp-img>
  <p class="img-caption">
    @if(!empty($content['caption']))
    SOLOPOS.COM - {{ htmlentities($content['caption']) }}
    @else
    SOLOPOS.COM - Panduan Informasi dan Inspirasi
    @endif
  </p> 
  @endif 
  <div class="news-box-content">  
      <div class="container mb-5 center-text">
        <amp-social-share type="facebook" width="64" height="24" class="facebook-bg"></amp-social-share>
        <amp-social-share type="twitter" width="64" height="24" class="twitter-bg"></amp-social-share>
        <amp-social-share type="whatsapp" width="64" height="24" class="whatsapp-bg"></amp-social-share>
        <amp-social-share type="email" width="64" height="24" class="mail-bg"></amp-social-share>
      </div>
      {{-- <div class="addthis-wrapper">
        <amp-addthis width="320" height="92"  data-pub-id="ra-4dc8efb674777330" data-widget-id="thr1" data-widget-type="inline"></amp-addthis>
      </div> --}}
      @php
      $konten = Helper::ampify(htmlspecialchars_decode($content['content'])) ;
      $contents = explode('</p>', $konten);
      $total_p = count(array_filter($contents)); 

      if($total_p > 13 && $total_p < 25 ): 
      $p_iklan_1  = array_slice($contents, 0, 3);
      $p_iklan_2  = array_slice($contents, 3, 5);
      $p_iklan_3  = array_slice($contents, 8, 5);						
      $last_p  = array_slice($contents, 13);

      elseif ($total_p > 25 && $total_p < 40 ) :
      $p_iklan_1  = array_slice($contents, 0, 3);
      $p_iklan_2  = array_slice($contents, 3, 6);
      $p_iklan_3  = array_slice($contents, 9, 7);
      $p_iklan_4  = array_slice($contents, 16, 7);						
      $last_p  = array_slice($contents, 23);

      elseif ($total_p > 40 ) :
      $p_iklan_1  = array_slice($contents, 0, 5);
      $p_iklan_2  = array_slice($contents, 5, 9);
      $p_iklan_3  = array_slice($contents, 14, 9);	
      $p_iklan_4  = array_slice($contents, 23, 9);
      $p_iklan_5  = array_slice($contents, 32, 9);	
      $p_iklan_6  = array_slice($contents, 41, 9);
      $p_iklan_7  = array_slice($contents, 50, 9);				
      $last_p  = array_slice($contents, 59);

      else :
      $p_iklan_1  = array_slice($contents, 0, 2);
      $p_iklan_2  = array_slice($contents, 2, 4);
      $p_iklan_3  = array_slice($contents, 6, 4);						
      $last_p  = array_slice($contents, 10);
      endif;
      @endphp 

      <!-- ads parallax -->
      
      {!! implode('</p>', $p_iklan_1) !!}

      {{-- <amp-embed class="half-top" width="600" height="600" layout="responsive" type="mgid" data-publisher="solopos.com" data-widget="990864" data-container="M628318ScriptRootC990864" > </amp-embed> --}}
      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {!! implode('</p>', $p_iklan_2) !!}

      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {!! implode('</p>', $p_iklan_3) !!}

      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-2449643854165381"
        data-ad-slot="3798913759"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>
      @if($total_p > 25 && $total_p < 40 )
      {!! implode('</p>', $p_iklan_4) !!}

      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>      
      @elseif($total_p > 40  )
      {!! implode('</p>', $p_iklan_4) !!}
      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {!! implode('</p>', $p_iklan_5) !!}
      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {!! implode('</p>', $p_iklan_6) !!}
      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {!! implode('</p>', $p_iklan_7) !!}
      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>
            
      @endif

      {!! implode('</p>', $last_p) !!}   

      {{-- <amp-embed type="dable" data-widget-id="Ylj0r3oO"
      data-service-name="m.solopos.com"
      data-item-id="2015111202028"
      height="650"></amp-embed> --}}

      {{-- <div class="profile-content-wrapper">
        <!-- User Meta Data-->
        <div class="user-meta-data">
          <!-- User Thumbnail-->
          <div class="user-thumbnail">
                @if(!empty($content['avatar']))
                <amp-img src="{{ htmlentities($content['avatar']) }}" width="80" height="80" alt="Profile"></amp-img>
                @else
                <amp-img src="https://images.solopos.com/2019/10/avatar-solopos-370x370.jpg" width="80" height="80" alt="Profile"></amp-img>
                @endif      
          </div>
          <!-- User Content-->             
          <div class="user-content">
            <h6>
              <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://www.solopos.com/arsip @endif">{{ $content['editor'] }}</a>
            </h6>
            
            <p>Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.</p>
            
            <div class="post-list">
              <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://www.solopos.com/arsip @endif">Lihat Artikel Saya Lainnya</a>
            </div>
            <div class="author-social">
              <span>Follow Me: </span>
              <a href="https://www.facebook.com/sharer/sharer.php?u=" target="_blank" rel="noopener"><i class="fa fa-facebook"></i></a>
				      <a href="https://twitter.com/home?status=" target="_blank" rel="noopener"><i class="fa fa-twitter"></i></a>
				      <a href="#"><i class="fa fa-instagram"></i></a>
				      <a href="whatsapp://send?text=** |  |  _selengkapnya baca di sini_ "><i class="fa fa-whatsapp"></i></a>
				      <a href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a>
            </div>              
          </div>
        </div>
      </div>       --}}
      <div class="container mb-5 center-text">
            <amp-social-share type="facebook" width="64" height="24" class="facebook-bg"></amp-social-share>
            <amp-social-share type="twitter" width="64" height="24" class="twitter-bg"></amp-social-share>
            <amp-social-share type="whatsapp" width="64" height="24" class="whatsapp-bg"></amp-social-share>
            <amp-social-share type="email" width="64" height="24" class="mail-bg"></amp-social-share>
      </div>
      <div class="decoration"></div>
      <div class="container mt-3">
        <div class="tag-lists">
        <span>Kata Kunci :</span>
        @if(isset($header['arrayTag']))
            @foreach($header['arrayTag'] as $tag)
            @php
                $tag_name = ucwords($tag);
                $tag_slug = str_replace(' ', '-',$tag);
                $tag_slug = strtolower($tag_slug)
            @endphp
            <a href="{{ url("/tag/{$tag_slug}") }}/amp">{{ $tag_name }}</a>
            @endforeach
        @endif                                
        </div>
      </div>

      <div class="iklan half-bottom" align="center">
        <amp-ad width="100vw" height="320"
        type="adsense"
        data-ad-client="ca-pub-2449643854165381"
        data-ad-slot="3798913759"
        data-auto-format="rspv"
        data-full-width="">
        <div overflow=""></div>
        </amp-ad>
      </div>

      {{-- <amp-ad width="600" height="600" layout="responsive" type="mgid" data-publisher="solopos.com" data-widget="990869" data-container="M628318ScriptRootC990869" > </amp-ad> --}}
            
      <!-- start hanya untukmu -->
      <h2 class="uppercase full-top no-bottom">Hanya Untuk Anda</h2>
      <h6 class="uppercase full-bottom color-red-dark">Inspiratif & Informatif</h6>       
      <div class="news-top half-bottom">
        @php $hl_loop = 1; @endphp
        @foreach($breakingcat as $hl)
        @if($hl_loop <= 15)       
        @if($hl_loop == 3)
        <div class="iklan half-bottom" align="center">
          <amp-ad width="100vw" height="320"
          type="adsense"
          data-ad-client="ca-pub-4969077794908710"
          data-ad-slot="2921244965"
          data-auto-format="rspv"
          data-full-width="">
          <div overflow=""></div>
          </amp-ad>
        </div>
        @endif        
        <a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}/amp" title="{{ html_entity_decode($hl['title']) }}" class="news-header">
            <amp-img src="{{ $hl['images']['thumbnail'] }}" layout="responsive" width="600" height="400" alt="{{ html_entity_decode($hl['title']) }}"></amp-img>
            <u class="bg-red-dark">HEADLINE</u>
            <i><span>{{ $hl['category'] }}</span></i>
            <strong>{{ html_entity_decode($hl['title']) }}</strong>
            <em>{{ Helper::time_ago($hl['date']) }}</em>
        </a>
        <amp-accordion class="news-share">
            <section>
                <h4><i class="fa fa-retweet"></i></h4>
                <p>
                    <amp-social-share type="facebook" width="43" height="40" class="custom-news-share"><i class="fa fa-facebook"></i></amp-social-share>
                    <amp-social-share type="twitter" width="43" height="40" class="custom-news-share"><i class="fa fa-twitter"></i></amp-social-share>
                    <amp-social-share type="pinterest" width="43" height="40" class="custom-news-share"><i class="fa fa-pinterest"></i></amp-social-share>
                    <amp-social-share type="linkedin" width="43" height="40" class="custom-news-share"><i class="fa fa-linkedin"></i></amp-social-share>
                    <amp-social-share type="email" width="43" height="40" class="custom-news-share"><i class="fa fa-envelope-o"></i></amp-social-share>
                </p>
            </section>
        </amp-accordion>
        @endif
        @php $hl_loop++ @endphp
        @endforeach    

        <div class="iklan half-bottom" align="center">
          <amp-ad width="100vw" height="320"
          type="adsense"
          data-ad-client="ca-pub-4969077794908710"
          data-ad-slot="2921244965"
          data-auto-format="rspv"
          data-full-width="">
          <div overflow=""></div>
          </amp-ad>
        </div>

    </div>	         
  </div> <!-- end blog content -->
</div>
{{-- <amp-img src="https://api.solopos.com/set-view?id={{ $content['id'] }}" width="1" height="1" alt="view"></amp-img> --}}
@endsection