@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="{{ url("") }}"><i class="fa fa-home"></i></a>
						</li>
						<li><i class="fa fa-angle-right"></i>Arsip Berita</li>
					</ol>		
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->
	
	<style>
		#hideMe {
			-moz-animation: cssAnimation 0s ease-in 12s forwards;
			/* Firefox */
			-webkit-animation: cssAnimation 0s ease-in 12s forwards;
			/* Safari and Chrome */
			-o-animation: cssAnimation 0s ease-in 12s forwards;
			/* Opera */
			animation: cssAnimation 0s ease-in 12s forwards;
			-webkit-animation-fill-mode: forwards;
			animation-fill-mode: forwards;
		}
		@keyframes cssAnimation {
			to {
				width:0;
				height:0;
				overflow:hidden;
			}
		}
		@-webkit-keyframes cssAnimation {
			to {
				width:0;
				height:0;
				visibility:hidden;
			}
		}	
		select {
		background-color: white;
		border: thin solid #00437d;
		border-radius: 4px;
		display: inline-block;
		width: 100%;
		font: inherit;
		line-height: 1.5em;
		padding: 0.5em 3.5em 0.5em 1em;
		margin: 0;      
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-appearance: none;
		-moz-appearance: none;
		}

		select.select-classic {
		background-image:
		linear-gradient(45deg, transparent 50%, #fff 50%),
		linear-gradient(135deg, #fff 50%, transparent 50%),
		linear-gradient(to right, #00437d, #00437d);
		background-position:
		calc(100% - 20px) calc(1em + 2px),
		calc(100% - 15px) calc(1em + 2px),
		100% 0;
		background-size:
		5px 5px,
		5px 5px,
		2.5em 2.5em;
		background-repeat: no-repeat;
		}

		select.select-classic:focus {
		background-image:
		linear-gradient(45deg, white 50%, transparent 50%),
		linear-gradient(135deg, transparent 50%, white 50%),
		linear-gradient(to right, gray, gray);
		background-position:
		calc(100% - 15px) 1em,
		calc(100% - 20px) 1em,
		100% 0;
		background-size:
		5px 5px,
		5px 5px,
		2.5em 2.5em;
		background-repeat: no-repeat;
		border-color: grey;
		outline: 0;
		}			
	</style>
	
    <!-- Section Terkini Start -->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<h2 class="block-title">
						<span class="title-angle-shap"> Halaman Arsip Berita Solopos.com </span>
					</h2>
					<div class="container">
						<div class="row">
							<div class="col-12">
							<form class="form-inline" action="{{ route('arsip') }}" method="post" name="sortindeks"> {{ csrf_field() }}
								<div class="form-group col-md-3 pr-0">{!! Form::select('tgl', array('01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'), $tgl, array('class' => 'select-classic'));  !!}</div>
								<div class="form-group col-md-4 pr-0">{!! Form::select('bln', array('1' => 'Januari', '2' => 'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), $bln, array('class' => 'select-classic')); !!}</div>
								<div class="form-group col-md-3 pr-0">{!! Form::selectRange('thn', 2009, 2022, $thn, array('class' => 'select-classic')); !!}</div>
								<div class="form-group col-md-2 pr-0"><input name="submit" type="submit" id="submit" value="GO" class="btn btn-primary btn-sm" style="padding: 7px;margin-left: 7px;margin-bottom: 7px;"/></div>
							</form>
							</div>
						</div>
					</div> 
					<div class="gap-30 d-none d-md-block"></div>

					<div class="row ts-gutter-20">
			          {{-- @php $no = 1; @endphp
			          @foreach ($breaking as $item)
						@php           
						$image = $item['featured_image']['thumbnail'] ?? '{{ url("/images/no-thumb.jpg") }}'; 
						$title = html_entity_decode($item['title']);
						@endphp 
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $image }}" alt="{{ $title }}" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
											<a class="post-cat-box {{ $item['catsname'] }}" href="">{{ $item['catsname'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											@if($no==1)
											<h1 class="post-title title-md">
												@if($item['is_premium'] == 'premium') 
												<span class="espos-plus">+ PLUS</span>
												@endif													
												<a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=arsip_desktop" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
											</h1>											
											@else 
											<h2 class="post-title title-md">
												@if($item['is_premium'] == 'premium') 
												<span class="espos-plus">+ PLUS</span>
												@endif													
												<a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=arsip_desktop" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
											</h2>											
											@endif
											<div class="post-meta mb-7">
												<!--<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($item['author']) {!! $item['author'] !!} @endif</a></span> -->
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Carbon\Carbon::parse($item['date'])->translatedFormat('l, j F Y H:i') }} WIB</span>
											</div>
											<p>@if($item['summary']) {!! $item['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                        @php $no++; @endphp
                        @endforeach	 --}}
						<div id="arsipPost"></div>
            			<p id="hideMe" style="font-size:15px; padding: 0 20px;">Mohon tunggu beberapa saat, kami perlu waktu untuk menampilkan semua data.</p>
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
				            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>									
				</div><!-- Content Col end -->

				<!-- sidebar home -->
				@include('includes.sidebar-category')

				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section>
    <!-- Section Terkini End -->	
	@push('custom-scripts')
    <script>
		function timeSince(date) {
			var seconds = Math.floor((new Date() - date) / 1000);

			var interval = seconds / 31536000;

			if (interval > 1) {
			return Math.floor(interval) + " tahun yang lalu";
			}
			interval = seconds / 2592000;
			if (interval > 1) {
			return Math.floor(interval) + " bulan yang lalu";
			}
			interval = seconds / 86400;
			if (interval > 1) {
			return Math.floor(interval) + " hari yang lalu";
			}
			interval = seconds / 3600;
			if (interval > 1) {
			return Math.floor(interval) + " jam yang lalu";
			}
			interval = seconds / 60;
			if (interval > 1) {
			return Math.floor(interval) + " menit yang lalu";
			}
			return Math.floor(seconds) + " detik yang lalu";
		}  		
		$(document).ready(function() {
			$.ajax({ //create an ajax request related post
			type: "GET",
			url: "https://api.solopos.com/api/breaking/arsip/posts?year={{ $thn }}&month={{ $bln }}&day={{ $tgl }}", 
			dataType: "JSON",
			success: function(data) {
				var arsipPosts = $("#arsipPost");
  
				$.each(data, function(i, item) {	
					var str = data[i]['is_premium'];
					if (str == 'premium'){
						plus =  '<!--<span class="espos-plus">+ PLUS</span>-->'
					} else {
						plus = ''
					}
					
				  arsipPosts.append("<div class=\"col-12 mb-10\"><div class=\"post-block-style\"><div class=\"row\"><div class=\"col-md-5\"><div class=\"post-thumb post-list_feed\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title'] + "\"><img src=\"" + data[i]['featured_image']['medium'] +"\" loading=\"lazy\" alt=\"" + data[i]['title'] + "\" style=\"object-fit: cover; height: 167px; width: 250px;\" onerror=\"javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'\"></a><a class=\"post-cat-box " + data[i]['catsname'] + "\" href=\"\">" + data[i]['catsname'] + "</a></div></div><div class=\"col-md-7 pl-0\"><div class=\"post-content\"><h2 class=\"post-title title-md\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title'] + "\">" + plus + " "  + data[i]['title'] + "</a></h2><div class=\"post-meta mb-7\"><span class=\"post-date\">" + data[i]['catsname'] + "  -  " + timeSince(new Date(data[i]['date'])) + "</span></div><p>"+ data[i]['summary'] +"</p></div></div></div></div></div>");
				});
					}
				});
			});
	</script>
	@endpush
@endsection