@extends('layouts.app')
@section('content')

    <!-- Section Terkini Start -->
	<section class="block-wrapper">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">
					
						<div class="owl-carousel owl-theme featured-slider h2-feature-slider">					
						@php $hl_loop = 1; @endphp
						@foreach($artikel as $hl) @if($hl_loop <=5)
							<div class="item post-overaly-style post-md" style="background-image:url({{ $hl['featured_image']['medium'] }})">
								<div class="featured-post">
									<a class="image-link" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">&nbsp;</a>
									<div class="overlay-post-content">
										<div class="post-content">
											<div class="grid-category">
												<a class="post-cat {{ $hl['catslug'] }}" href="{{ url("/rsjihsolo/artikel") }}">{{ $hl['catsname'] }}</a>
											</div>
	
											<h2 class="post-title title-md">
												<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
											</h2>
											<div class="post-meta">
												<ul>
													<li><a href="#"><i class="fa fa-user"></i> @if($hl['author']) {!! $hl['author'] !!} @endif</a></li>
													<li><a href="#"><i class="icon icon-clock"></i> {{ Helper::time_ago($hl['date']) }}</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div><!--/ Featured post end -->
							</div><!-- Item 1 end -->
							
						@endif @php $hl_loop++; @endphp @endforeach   
						</div>
					<div class="gap-20"></div>

					<h2 class="block-title">
						<span class="title-angle-shap"> Artikel Bugar Terkini </span>
					</h2>
					<div class="row ts-gutter-20 align-items-center">
			          @php $no = 1; @endphp
			          @foreach($artikel as $post) @if($no <= 3 )

			          @if($no==3)
			          <!-- ads advertorial -->
			          @endif
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $post['featured_image']['thumbnail'] }}" alt="{{ html_entity_decode($post['title']) }}" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box {{ $post['catslug'] }}" href="{{ url("/rsjihsolo/artikel") }}">{{ $post['catsname'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($post['author']) {!! $post['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
											</div>
											<p>@if($post['summary']) {!! $post['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					  @endif @php $no++; @endphp @endforeach 	
					</div>

					<div class="row ts-gutter-20 loadmore-frame">
			          @php $no = 1; @endphp
			          @foreach($artikel as $post) @if($no > 3)
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $post['featured_image']['thumbnail'] }}" alt="{{ html_entity_decode($post['title']) }}" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box {{ $post['catslug'] }}" href="{{ url("/rsjihsolo/artikel") }}">{{ $post['catsname'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>@if($post['author']) {!! $post['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
											</div>
											<p>@if($post['summary']) {!! $post['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
                        @php $no++; @endphp
                        @endforeach	
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
				            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>									
				</div><!-- Content Col end -->

				<!-- sidebar home -->
				@include('includes.sidebar-bugar')

				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section>
    <!-- Section Terkini End -->


@endsection