@extends('layouts.app')
@section('content')
    <section class="featured-post-area no-padding">
        <div class="container pl-0 pr-0">
			@if(Cookie::get('is_login') == 'true')
			<div class="author-box d-flex alert alert-primary alert-dismissable" style="padding-left:30px; padding-right:30px;margin-top:0;">
				<div class="author-info" style="margin-left:0;margin-right:30px;">
					<h6 style="margin-top:0;font-size:14px;font-weight:400">
						<a href="https://id.solopos.com/profile">{{ Helper::greetings() }} <strong>{{ Cookie::get('is_name') }}</strong>, Bagaimana Kabarnya?</a>
					</h6>
					<p style="font-size:13px; line-height:19px;padding-top:10px;">Di ekosistem Solopos Media Group kami percaya member harus jadi prioritas utama. Hubungi Customer Service kami dengan mengklik nomer ini <strong><a href="https://api.whatsapp.com/send/?phone=6289510318382&text&type=phone_number&app_absent=0" title="Kontak Admin">089510318382 (WhatsApp)</a></strong> apabila Anda memiliki pertanyaan, saran, kritik, atau mengalami kendala terkait layanan Espos Plus. Kami ada untuk memberikan yang terbaik bagi Anda.</p>
				</div>
				<div class="align-items-center">
					<img class="align-bottom" src="https://id.solopos.com/images/logo.png" width="100" alt="Solopos.id">
					<div class="author-info" style="margin-left:0;">
						<div style="margin-top:15px; text-align:center;">
							<button class="btn btn-success btn-sm" style="padding:.25rem 1rem;"><a href="https://id.solopos.com" style="color:#fff;">Kelola Akun</a></button>
						</div>
					</div>
				</div>
			</div>	
			@endif	
			<section class="trending-slider pt-lg-0">
				<div class="container pr-0 pl-0">
					<div class="ts-grid-box">
						<div class="owl-carousel dot-style2 transing-slide-style2">
							@php $hl_loop = 1; @endphp
							@foreach($premium as $hl) 
							@if($hl_loop <=7)							
							<div class="item post-overaly-style post-lg" style="background-image:url({{ $hl['images']['thumbnail'] }})">
								<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}" class="image-link">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat" href="{{ url("/{$hl['category']}") }}" style="border-radius: 5px;">{{ $hl['category'] }} <span style="color:#f39e21;font-weight:700;"> + PLUS</span></a>
										</div>
		
										<h2 class="post-title title-md">
											<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
												{{ html_entity_decode($hl['title']) }}
											</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>@if($hl['author']) {!! $hl['author'] !!} @endif</a></li>
												<li><a href="#"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($hl['date']) }}</a></li>
												{{-- <li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li> --}}
											</ul>
										</div>
									</div>
								</div>
							</div><!-- Item end -->
							@endif 
							@php $hl_loop++; @endphp 
							@endforeach							
						</div><!-- owl end-->
					</div><!-- post-box end-->
				</div><!-- container end-->
			</section>					
            {{-- <div class="row ts-gutter-20">
                <div class="col-lg-8 col-md-12 pad-r">
                    <div class="owl-carousel owl-theme featured-slider h2-feature-slider">					
                    @php $hl_loop = 1; @endphp
                    @foreach($premium as $hl) 
                        @if($hl_loop <=5)
                        <div class="item post-overaly-style post-md" style="background-image:url({{ $hl['images']['thumbnail'] }})">
                            <div class="featured-post">
                                <a class="image-link" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">&nbsp;</a>
                                <div class="overlay-post-content">
                                    <div class="post-content">
                                        <div class="grid-category">
                                            <a class="post-cat {{ $hl['category'] }}" href="{{ url("/{$hl['category']}") }}">{{ $hl['category'] }}</a>
                                        </div>

                                        <h2 class="post-title title-md">
                                            <span class="espos-plus">+ PLUS</span>
											<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
                                        </h2>
                                        <div class="post-meta">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-user"></i> @if($hl['author']) {!! $hl['author'] !!} @endif</a></li>
                                                <li><a href="#"><i class="icon icon-clock"></i> {{ Helper::time_ago($hl['date']) }}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!--/ Featured post end -->
                        </div><!-- Item 1 end -->
                        
                    @endif 
                    @php $hl_loop++; @endphp 
                    @endforeach   
                    </div>
                </div><!-- Col 8 end -->
                @php $pc_loop = 1; @endphp
                @foreach($premium as $pc) 
                @if($pc_loop == 6)
                <div class="col-lg-4 col-md-12">
                    <div class="post-overaly-style post-md" style="background-image:url({{ $pc['images']['thumbnail'] }})">
                        <div class="featured-post">
                            <a class="image-link" href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ html_entity_decode($pc['title']) }}">&nbsp;</a>
                            <div class="overlay-post-content">
                                <div class="post-content">
                                    <h2 class="post-title title-md">
										<span class="espos-plus">+ PLUS</span>
                                        <a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
                                    </h2>
                                    <div class="post-meta">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></li>
                                            <li><a href="#"><i class="icon icon-clock"></i>{{ Helper::time_ago($pc['date']) }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!--/ Featured post end -->
                    </div><!-- Item 2 end -->
                </div><!-- Col 4 end -->
                @endif @php $pc_loop++; @endphp @endforeach


            </div><!-- Row end --> --}}
        </div><!-- Container end -->
    </section><!-- Feature post end -->

	<section class="block pt-0">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title" style="color:#f39e21;">
						<span class="title-angle-shap"> Espos Plus</span>
					</h2>
				</div>
				@php $hl_loop = 1; @endphp
				@foreach($premium as $hl) 
				@if($hl_loop > 7 && $hl_loop <= 11)				
				<div class="col-md-3">
					<div class="post-block-style">
						<div class="post-thumb">
							<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
								<img class="img-fluid" src="{{ $hl['images']['thumbnail'] }}" alt="{{ html_entity_decode($hl['title']) }}" width="215" height="143" style="object-fit: cover; width: 215px; height: 143px;">
							</a>
							<div class="grid-cat">
								<a class="post-cat" href="{{ url("/{$hl['category']}") }}" style="border-radius: 5px;">{{ $hl['category'] }} <span style="color:#f39e21;font-weight:700;"> + PLUS</span></a>
							</div>
						</div>
						
						<div class="post-content">
							<h2 class="post-title">
								<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
							</h2>
							{{-- <div class="post-meta mb-7">
								<span class="post-author"><a href="#"><i class="fa fa-user"></i>@if($hl['author']) {!! $hl['author'] !!} @endif</a></span>
								<span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
							</div> --}}
						</div><!-- Post content end -->
					</div><!-- post-block -->
				</div><!-- col end -->
				@endif 
				@php $hl_loop++; @endphp 
				@endforeach					
			</div><!-- row end -->
		</div>
	</section>

    {{-- <div class="gap-20"></div> --}}
    <!-- ads top leaderboard -->

    <!-- Section Terkini Start -->
	<section class="block-wrapper" style="padding-top:20px;">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">

					@if(Cookie::get('is_login') == 'true')
					<div id="konten-premium" class="premium">									
						@if(Cookie::get('is_membership') == 'free')
						<div class="subscribe mx-auto">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<div class="header-box">
										<img class="logo" src="{{ url('images/plus.png') }}" alt="logo">
										<div class="login">
											Hi, silahkan <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a> Anda.
										</div>
									</div>
									
									<div class="top-box">
										<div class="lead">Langganan Espos Plus</div>
										<div class="sublead">
											Hi {{ Cookie::get('is_name') }}, terima kasih sudah menjadi bagian dari Espos Plus. Silakan <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a> berlangganan dan dapatkan berbagai konten menarik di Espos Plus.
										  </div>
									</div>
									<div class="button-container">
										<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;">Langganan Sekarang, Dapat Mobil !</a>
										<a href="https://www.solopos.com/page/help" target="_blank">PELAJARI SELENGKAPNYA</a>
									</div>
									<div class="footer-box">
										<span class="callout">*Syarat & Kententuan berlaku</span> 
									</div>	
								</div>
							</div>
						</div>
						<div>
							<img src="https://cdn.solopos.com/plus/espos-plus-desktop.jpg" width="100%">
						</div>
						<div class="gap-30"></div>						
						@endif
					</div>	
					@else
					<div class="subscribe mx-auto">
						<div class="row justify-content-center">
							<div class="col-md-12">
								<div class="header-box">
									<img class="logo" src="{{ url('images/plus.png') }}" alt="logo">
									<div class="login">
										Sudah Berlangganan ? <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="login">Login <i class="fa fa-arrow-circle-o-right"></i></a>
									</div>
								</div>
								<div class="top-box">
									<div class="lead">Langganan Espos Plus</div>
									<div class="sublead">Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.</div>
								</div>
								<div class="button-container">
									<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;">Langganan Sekarang, Dapat Mobil !</a> 
									<a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="Login" class="checkout-button inverse">LOGIN | DAFTAR</a>
									<a href="https://www.solopos.com/page/help" target="_blank">PELAJARI SELENGKAPNYA</a>
								</div>
								<div class="footer-box">
									<span class="callout">*Syarat & Kententuan berlaku</span> 
								  </div>
							</div>
						</div>
					</div>
					<div>
						<img src="https://cdn.solopos.com/plus/espos-plus-desktop.jpg" width="100%">
					</div>
					<div class="gap-30"></div>							
					@endif

                    <!-- Block Konten Premium end -->	
					<h2 class="block-title" style="color:#f39e21;">
						<span class="title-angle-shap"> Espos Plus Terbaru </span>
					</h2>
					<div class="row ts-gutter-20 loadmore-frame">
			          @php $no = 1; @endphp
			          @foreach($premium as $post)
                        @if($no > 11)
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $post['images']['url_thumb'] }}" alt="{{ $post['title'] }}" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box {{ $post['category'] }}" href="https://www.solopos.com/{{ $post['category'] }}">{{ $post['category'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											@if($no==12)
											<h1 class="post-title title-md">
												<span class="espos-plus">+ PLUS</span>
												<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h1>											
											@else 
											<h2 class="post-title title-md">
												<span class="espos-plus">+ PLUS</span>
												<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h2>										
											@endif
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($post['author']) {!! $post['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
											</div>
											<p>@if($post['summary']) {!! $post['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                        @endif
                        @php $no++ @endphp
                        @endforeach	
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita" style="background:#f39e21;">Espos Plus Lainnya...</a>
				            <a href="https://www.solopos.com/arsip-plus" class="btn btn-primary btn-sm load-more-arsip" style="display: none;background:#f39e21;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>	

                    <!-- Block Konten Premium -->
					{{-- <div class="block style2 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> ESPOS PLUS</span>
						</h2>

						<div class="row">
				            @php $pc_loop = 1; @endphp
				            @foreach($premium as $pc) @if($pc_loop <= 12)

				            @if($pc_loop == 7)
							<div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=premium_desktop" title="{{ $pc['title'] }}">
											<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; width: 266px; height: 178px;">
										</a>
									</div>
									
									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											<span style="position:relative; height: 20px; line-height:21px; display:inline-block;color:#fff;background:#00a857/*#00a857 #05abd9*/;padding:0 7px!important;font-size: 12px; font-weight:600;border-radius:3px;">+ PLUS</span>
											<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
										</h2>
										<p>@if($pc['summary']) {!! $pc['summary'] !!} @endif</p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($pc['date']) }}</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">								
							@endif

							@if( $pc_loop > 7 && $pc_loop <= 11 )

									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ $pc['title'] }}">
													<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<span style="position:relative; height: 15px; line-height:16px; display:inline-block;color:#fff;background:#00a857/*#00a857 #05abd9*/;padding:0 3px!important;font-size: 10px; font-weight:400;border-radius:3px;">+ PLUS</span>
													<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
							@endif
							@if($pc_loop == 11)
								</div><!-- .row -->
							</div><!-- Col 2 end -->
							@endif
							@endif
							@php $pc_loop++; @endphp
							@endforeach
						</div><!-- Row end -->
					</div> --}}
				</div><!-- Content Col end -->

				<!-- sidebar home -->
				@include('includes.sidebar-premium')

				<!-- Sidebar Col end -->
			</div><!-- Row end -->

		</div><!-- Container end -->
	</section>
    <!-- Section Terkini End -->
	<style>
		.subscribe {
			background-color: #fff;
			margin-top: 20px;
			margin-bottom: 50px;
			position: relative;
			border: 1px solid #00437d;
			border-radius: 10px;
		}	
		.header-box {
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			align-items: center;
			padding: 30px 30px 10px;
		}																
		.top-box {
			display: flex;
			flex-direction: column;
			justify-content: center;
			padding: 0 30px;
		}									
		.header-box .logo {
			width: 150px;
		}
		.header-box .login {
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			font-size: 14px;
			font-weight: 600;
			text-align: center;
			letter-spacing: 0.02em;
			color: #f29b27;
		}
		.subscribe .lead {
			font-size: 27px;
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			line-height: 34px;
			font-weight: 700;
			text-align: center;
			letter-spacing: -0.01em;
			color: #2E2E2E;
			margin: 5px auto 10px auto;
		}
		.subscribe .sublead {
			text-align: center;
			margin: 0 auto 15px auto;
			font-size: 12px;
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			line-height: 21px;
		}	
		.button-container {
			display: flex;
			flex-direction: column;
			justify-content: center;
			margin: 20px auto;
		}										
		.checkout-button {
			display: inline-block;
			width: 325px;
			height: 40px;
			padding: 8px 32px;
			border: 1px solid #00437d;
			border-radius: 4px;
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			font-weight: bold;
			font-size: 14px;
			letter-spacing: 0.05em;
			text-transform: uppercase;
			text-align: center;
			color: #FFFFFF !important;
			background: #00437d;
			margin: 0 auto 15px auto;
		}	
		.inverse {
			color: #00437d !important;
			background: #FFFFFF;
			border: 1px solid #00437d;
		}										
		.button-container a {
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			font-size: 14px;
			font-weight: bold;
			text-align: center;
			letter-spacing: 0.02em;
			text-decoration: none;
		}		
		.footer-box {
			display: flex;
			flex-direction: row;
			justify-content: flex-start;
			padding: 30px;
		}	
		.callout {
			display: block;
			font-family: 'Roboto', sans-serif;
			font-weight: 500;
			font-size: 9px;
			line-height: 9px;
			text-transform: uppercase;
			letter-spacing: 0.05em;
			opacity: 0.7;
		}	
		.owl-carousel .owl-nav .owl-prev, .owl-carousel .owl-nav .owl-next, .trending-bar {background: #f29b27;}																										
	</style>
			
@endsection