@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
    <!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li><a href="{{ url('/') }}/foto">Foto</a> <i class="fa fa-angle-right"></i></li>
						<li>{{ $content['title'] }}</li>
					</ol>
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->
	<!-- mfunc setPostViews(get_the_ID()); --><!--/mfunc-->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<div class="single-post">
						<div class="post-header-area">
							<h1 class="post-title title-lg title-only">{{ $header['title'] }}</h1>
							<p>{{ $header['ringkasan'] }}</p>
							<ul class="post-meta">
								<li>
									<i class="fa fa-folder-open"></i>  <a href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a>
								</li>
								<li><i class="fa fa-clock-o"></i> {{ Helper::indo_datetime($content['date']) }} WIB</li><br>
								<li>
									@if($header['author'] != $header['editor'] )
										Penulis:
										@foreach ($author_slug as $author)
										<a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif" class="penulis">{{ ucwords(str_replace('_', ' ', $author)) }}</a> <span style="margin-right:3px; margin-left:3px;padding-right:0;">|</span>
										@endforeach
									@endif
									Editor:  <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="penulis" target="_blank">{{ $header['editor'] }}</a>
								</li>
								<li class="social-share">
									<i class="shareicon fa fa-share"></i>
									<ul class="social-list">
										<li><a data-social="facebook" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a></li>
										<li><a data-social="twitter" class="twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a></li>
										<li><a data-social="whatsapp" class="whatsapp" href="https://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a></li>
										{{-- <li><a data-social="email" class="email" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a></li> --}}
									</ul>
								</li>
							</ul>
						</div><!-- post-header-area end -->
						<div class="post-content-area">

                            <style>
                                figure, .wp-caption {display: none !important;}
                            </style>
							@php
							$image = Helper::konten(htmlspecialchars_decode($content['content'])) ;
							$images = explode('</figure>', $image);
							$total_img = count(array_filter($images));

							$text = Helper::konten(htmlspecialchars_decode($content['content']));
							preg_match_all( '@src="([^"]+)"@' , $text, $img );
							preg_match_all( '/<figcaption (.*?)>(.*?)<\/figcaption>/s' , $text, $capt );
							$src = array_pop($img);
							$captions = array_pop($capt);
							@endphp

							@if($total_img == 1 )
							<div class="post-media mb-20">
								<a href="{{ $content['image'] }}" class="gallery-popup cboxElement">
									<img src="{{ $content['image'] }}" alt="@if(!empty($content['caption'])) {{ html_entity_decode($content['caption']) }} @endif" class="img-fluid">
								</a>
								<span>
                                    @if(!empty($content['caption']))
                                        <p>SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}</p>
                                    @else
                                        <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
                                    @endif
                                </span>
							</div>
							@else
                            <div class="owl-carousel owl-theme featured-slider h2-feature-slider">
                                <div class="post-media mb-20">
                                    <a href="{{ $content['image'] }}" class="gallery-popup cboxElement">
                                        <img src="{{ $content['image'] }}" alt="@if(!empty($content['caption'])) {{ html_entity_decode($content['caption']) }} @endif" class="img-fluid">
                                    </a>
                                    <span>
                                        @if(!empty($content['caption']))
                                        <p>SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}</p>
                                        @else
                                        <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
                                        @endif
                                    </span>
                                </div>
                                @foreach (array_combine($src, $captions) as $src => $captions)
                                <div class="post-media mb-20">
                                    <a href="{{$src}}" class="gallery-popup cboxElement">
                                        <img src="{{$src}}" alt="" class="img-fluid">
                                    </a>
                                    <span>
                                        <p>SOLOPOS.COM - {{$captions}}</p>
                                    </span>
                                </div>
                                @endforeach
                            </div>
							@endif

							<div class="iklan mt-3 mb-3" align="center">
								<!-- Custom Long Period Ads -->
								<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
								<script>
								window.googletag = window.googletag || {cmd: []};
								googletag.cmd.push(function() {
									googletag.defineSlot('/54058497/sukun-desktop', [640, 200], 'div-gpt-ad-1633002605726-0').addService(googletag.pubads());
									googletag.pubads().enableSingleRequest();
									googletag.enableServices();
								});
								</script>
								<!-- /54058497/sukun-desktop -->
								<div id='div-gpt-ad-1633002605726-0' style='min-width: 640px; min-height: 200px;'>
									<script>
									googletag.cmd.push(function() { googletag.display('div-gpt-ad-1633002605726-0'); });
									</script>
								</div>
							</div>

							@php
							$konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
							$contents = explode('</p>', $konten);
							$total_p = count(array_filter($contents));

							if($total_p > 1 ):
							$p_promosi  = array_slice($contents, 0, 1);
							$p_iklan_1  = array_slice($contents, 1, 3);
							$last_p  = array_slice($contents, 5);
							endif;
							@endphp
							<!-- ads top -->

							<!-- ads parallax -->
							{!! implode('</p>', $p_promosi) !!}
							<p><em><strong><span>Promosi</span></span><a href="https://www.solopos.com/{{ $promosi['slug'] }}-{{ $promosi['id'] }}?utm_source=read_promote" title="{{ $promosi['title'] }}" target="_blank" rel="noopener">{{ $promosi['title'] }}</a></strong></em>
							</p>

							{!! implode('</p>', $p_iklan_1) !!}
							<div class="iklan mt-3 mb-3" align="center">
								<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
								<ins class="adsbygoogle"
									style="display:block; text-align:center;"
									data-ad-layout="in-article"
									data-ad-format="fluid"
									data-ad-client="ca-pub-4969077794908710"
									data-ad-slot="6460499125"></ins>
								<script>
									(adsbygoogle = window.adsbygoogle || []).push({});
								</script>
							</div>

							{!! implode('</p>', $last_p) !!}
							<div align="center" style="margin: 20px 0;">
								@include('includes.ads.last-paragraf-article')
							</div>

							@if( date('Y-m-d H:i:s') >= '2021-12-23 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-28 23:59:59')
							<video width="100%" autoplay loop controls muted>
								<source src="https://cdn.solopos.com/iklan/ONLINE_SASA_SANTAN.mp4" type="video/mp4">
							Your browser does not support the video.
							</video>
							<br>
							@elseif( date('Y-m-d H:i:s') >= '2021-11-12 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-12 23:59:59')
							<a href="https://linktr.ee/EsportArena" target="_blank"><video width="100%" autoplay loop controls muted>
								<source src="{{ url('images/banner/ESPORT_JOGJA.mp4') }}" type="video/mp4">
							Your browser does not support the video.
							</video></a><br>
							@endif

						</div><!-- post-content-area end -->
						@php if($header['source'] != ''): @endphp
						<b>Sumber:</b> {{ $header['source'] }}
						@php endif @endphp

						@include('includes.ads.internal-promotion')

						@include('includes.ads.video-daylimotion')
						
				        <div class="social-share-btn d-flex align-items-center flex-wrap">
				          SHARE :
				          <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a>
				          <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a>
				          <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="fa fa-instagram"></i></a>
				          <a class="btn-whatsapp" href="whttps://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a>
				          <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a>
				        </div>

						<div class="post-footer">
							<div class="tag-lists">
								<span>Tags: </span>
                                @if(isset($header['arrayTag']) AND empty($content['tag'][0]['term_id']))
									@foreach($header['arrayTag'] as $tag)
									@php
										$tag_name = ucwords($tag);
										$tag_slug = str_replace(' ', '-',$tag);
										$tag_slug = strtolower($tag_slug)
									@endphp
										<a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
									@endforeach
								@else
									@foreach($header['arrayTag'] as $tag)
										<a href="{{ url("/tag/{$tag['slug']}") }}">{{ $tag['name'] }}</a>
									@endforeach
        						@endif
							</div><!-- tag lists -->

						</div>

					</div><!-- single-post end -->

					@include('includes.ads.under-article')

					<!-- realted post end -->

					<div class="related-post">
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terkait </span>
						</h2>
						<div class="row" id="relposts">
                            <div class="col-md-4"><div class="post-block-style"><div class="post-thumb"><a class="post-title" href="tips-bangun-bisnis-yang-cocok-buat-kaum-rebahan-di-2022-anti-ribet-1336534?utm_source=bacajuga_desktop" title="Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!"><img src="https://images.solopos.com/2022/06/Dilayani-Tokopedia-2-1-150x100.jpg" alt="Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!" style="object-fit: cover; width: 195px; height: 128px;"></a><div class="grid-cat"><a class="post-cat bisnis" href="#">Bisnis</a></div></div><div class="post-content"><h2 class="post-title"><a class="post-title" href="tips-bangun-bisnis-yang-cocok-buat-kaum-rebahan-di-2022-anti-ribet-1336534?utm_source=bacajuga_desktop" title=""> Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!</a></h2><div class="post-meta mb-7 p-0"></div></div></div></div>

							<div class="col-md-4"><div class="post-block-style"><div class="post-thumb"><a class="post-title" href="mengupas-hipospadia-kelainan-bawaan-yang-bisa-dialami-anak-laki-laki-1336362?utm_source=bacajuga_desktop" title="Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki"><img src="https://images.solopos.com/2022/06/hispos-150x100.jpg" alt="Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki" style="object-fit: cover; width: 195px; height: 128px;"></a><div class="grid-cat"><a class="post-cat artikel" href="#">Bugar</a></div></div><div class="post-content"><h2 class="post-title"><a class="post-title" href="mengupas-hipospadia-kelainan-bawaan-yang-bisa-dialami-anak-laki-laki-1336362?utm_source=bacajuga_desktop" title=""> Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki</a></h2><div class="post-meta mb-7 p-0"></div></div></div></div>
                        </div>

					</div>

                    <div class="related-post mt-5">
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Lainnya </span>
						</h2>
						<div class="row">
                            <ol id="rekomendasi" class="harjo-widget">

                            </ol>
                        </div>
					</div>

					<div class="gap-30 d-none d-md-block"></div>

					<!-- Block Konten Premium -->
					<div class="block style2 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> Espos Plus</span>
						</h2>

						<div class="row">
				            @php $pc_loop = 1; @endphp
				            @foreach($premium as $pc) @if($pc_loop <= 5)

				            @if($pc_loop == 1)
							<div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">
											<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; object-position: center; width: 266px; height: 178px;">
										</a>
										<div class="grid-cat">
											<a class="post-cat premium" href="{{ url('/plus') }}">Espos Plus</a>
										</div>
									</div>

									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											<span class="espos-plus">+ PLUS</span>
											<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
										</h2>
										<p>{{ $pc['summary'] }}</p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i>{{ Helper::time_ago($pc['date']) }}</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">
                                @endif
                                @if( $pc_loop > 1 && $pc_loop <= 5 )
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">
													<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
												</a>
											</div>

											<div class="post-content">
												<h2 class="post-title mb-2">
													<span class="espos-plus">+ PLUS</span>
													<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
                                @endif
                                @if($pc_loop == 5)
								</div><!-- .row -->
							</div><!-- Col 2 end -->
						@endif @endif @php $pc_loop++; @endphp @endforeach
						</div><!-- Row end -->
					</div><!-- Block Konten Premium end -->
					<div class="gap-50 d-none d-md-block"></div>

					<div>
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terkini </span>
						</h2>
						<div class="row ts-gutter-20 loadmore-frame">
				          @php $no = 0; @endphp
				          @foreach($breakingcat as $posts)
						  @if($no <=15 )
							<div class="col-12 mb-10 content-box">
								<div class="post-block-style">
									<div class="row">
										<div class="col-md-5">
											<div class="post-thumb post-list_feed">
												<img src="{{ $posts['images']['thumbnail'] }}" alt="{{ html_entity_decode($posts['title']) }}" style="object-fit: cover; object-position: center; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
												<a class="post-cat-box {{ $posts['category'] }}" href="{{ url("/{$posts['category']}") }}">{{ $posts['category'] }}</a>
											</div>
										</div>
										<div class="col-md-7 pl-0">
											<div class="post-content">
												<h2 class="post-title title-md">
													<a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" title="{{ html_entity_decode($posts['title']) }}">{{ html_entity_decode($posts['title']) }}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($posts['author']) {!! $posts['author'] !!} @endif</a></span>
													<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($posts['date']) }}</span>
												</div>
												<p>@if($posts['summary']) {{ $posts['summary'] }} @endif</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif
						  @php $no++; @endphp
						  @endforeach
							<div class="col-12 mt-3 align-items-center" style="text-align: center;">
					            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Cek Berita Lainnya</a>
					            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
							</div><!-- col end -->
						</div>
					</div><!-- Content Col end -->
				</div><!-- col-lg-8 -->

				<!-- sidebar start -->
				@include('includes.sidebar-category')
				<!-- sidebar end -->
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->

	{{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
    @push('custom-scripts')
    <script>
		$(document).ready(function() {
				$.ajax({ //create an ajax request related post
				type: "GET",
				url: "https://api.solopos.com/api/wp/v2/posts?tags={{ $relatedTags }}&exclude={{ $content['id'] }}&per_page=4", //72325
				dataType: "JSON",
				success: function(data) {
					// console.log(data);
						var relatedPosts = $("#relposts");

						$.each(data, function(i, item) {
							relatedPosts.append("<div class=\"col-md-4\"><div class=\"post-block-style\"><div class=\"post-thumb\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=bacajuga_desktop\" title=\"" + data[i]['title']['rendered'] + "\"><img src=\"" + data[i]['one_call']['featured_list']['media_details']['sizes']['thumbnail']['source_url'] +"\" alt=\"" + data[i]['title']['rendered'] + "\" style=\"object-fit: cover; width: 195px; height: 128px;\"></a><div class=\"grid-cat\"><a class=\"post-cat " + data[i]['one_call']['categories_list'][0]['slug'] + " \" href=\"#\">" + data[i]['one_call']['categories_list'][0]['name'] + "</a></div></div><div class=\"post-content\"><h2 class=\"post-title\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=bacajuga_desktop\" title=\"\"> " + data[i]['title']['rendered'] + "</a></h2><div class=\"post-meta mb-7 p-0\"></div></div></div>");
						});
				}
			});
		});

        $(document).ready(function () {
            $.ajax({
                url: 'https://www.harianjogja.com/rss',
                type: 'GET',
                dataType: "xml"
            }).done(function(xml) {

                $.each($("item", xml), function(i, e) {

                    var postNumber = i + 1 + ". ";

                    var itemURL = ($(e).find("link"));
                    var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

                    // var itemImg = ($(e).find("enclosure"));
                    // var imgUrl = itemImg.getAttribute('url');

                    var itemTitle = ($(e).find("title"));
                    var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                    if(postNumber <= 5) {

                    $("#rekomendasi").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\" rel=\"noopener noreferrer\">" + itemTitle.text() +"</a></li>");

                    }
                });
            });
        });

		$(document).ready(function() {
				$.ajax({
				xhrFields: {
      				withCredentials: true
   				},
				crossDomain: true,
				cache: false,
				type: "GET",
				url: "https://id.solopos.com/is_login", //72325
				dataType: "JSON",
				success: function(data) {
					//alert(data);
					console.log(data);
				}
			});
		});

    </script>
	<script>

        document.oncopy = function () {
        var _title = $(".title-only").text(),
            _editor = $(".editor-only").text(),
            _author = $(".penulis").text(),
            _publish = $(".tgl-terbit").text(),
            _href = document.location.href,
                _result,
                _body = document.getElementsByTagName("body")[0];
                _text =
                    (nl2br(_result = window.getSelection())) +
                    '<br /><br /><br /> Baca artikel Solopos.com <b>"' + _title.trim() +
                    '"</b> selengkapnya di sini: <a href="' +
                    _href +
                    '">' +
                    _href +
                    '</a>.'+
                    '<br />Editor  : '+_editor+'<br />Penulis: '+_author+'<br />Publish: '+_publish+
                    '<br/><br/> Mau Mobil SUV Idaman hanya dengan Rp.328/hari? <a href="https://id.solopos.com/register">Langganan Espos Plus Sekarang</a><br/>Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi';

        if (window.clipboardData && window.clipboardData.setData) {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData(p);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {

            var ss = document.createElement("div");

                (ss.style.position = "absolute"),
                (ss.style.left = "-99999px"),
                _body.appendChild(ss),
                (ss.innerHTML = _text),
                _result.selectAllChildren(ss);
                window.setTimeout(function () {
                    _body.removeChild(ss);
                }, 0);
        	}
        };


        function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
        	return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
	</script>
    @endpush
    @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush


@endsection
