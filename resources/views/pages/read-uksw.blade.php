@extends('layouts.app')
@section('content')
    <!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>{{ $content['category'] }}</li>
						<li><i class="fa fa-angle-right"></i> {{ html_entity_decode($content['title']) }}</li>
					</ol>
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->
	<!-- mfunc setPostViews(get_the_ID()); --><!--/mfunc-->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<div class="single-post">
						<div class="post-header-area">
							<h1 class="post-title title-lg title-only">{{ $header['title'] }}</h1>
							<p>{{ $header['ringkasan'] }}</p>
							<ul class="post-meta">
								<li>
									<i class="fa fa-folder-open"></i>  <a href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a>
								</li>
								<li><i class="fa fa-clock-o"></i> {{ Helper::indo_datetime($content['date']) }} WIB</li><br>
								<li>
									@if($header['author'] != $header['editor'] )
										Penulis:
										@foreach ($author_slug as $author)
										<a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif" class="penulis">{{ ucwords(str_replace('_', ' ', $author)) }}</a> <span style="margin-right:3px; margin-left:3px;padding-right:0;">|</span>
										@endforeach
									@endif
									Editor:  <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="penulis" target="_blank">{{ $header['editor'] }}</a>
								</li>

								<li class="social-share">
									<i class="shareicon fa fa-share"></i>
									<ul class="social-list">
										<li><a data-social="facebook" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a></li>
										<li><a data-social="twitter" class="twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a></li>
										<li><a data-social="whatsapp" class="whatsapp" href="https://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a></li>
										{{-- <li><a data-social="email" class="email" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a></li> --}}
									</ul>
								</li>
							</ul>
						</div><!-- post-header-area end -->

						<div class="post-content-area">
							<div class="post-media mb-20">
								<a href="{{ $content['image'] }}" class="gallery-popup cboxElement">
									<img src="{{ $content['image'] }}" alt="@if(!empty($content['caption'])) {{ htmlentities($content['caption']) }} @endif" class="img-fluid">
								</a>
								<span>
									@if(!empty($content['caption']))
										<p>SOLOPOS.COM - {{ htmlentities($content['caption']) }}</p>
									@else
										<p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
									@endif
								</span>
							</div>

                             {!! htmlspecialchars_decode($content['content']) !!}

							 @include('includes.ads.internal-promotion')

							 @include('includes.ads.video-daylimotion')
						</div><!-- post-content-area end -->
						<div style="margin: 20px 0;">
                                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <ins class="adsbygoogle"
                                    style="display:block; text-align:center;"
                                    data-ad-layout="in-article"
                                    data-ad-format="fluid"
                                    data-ad-client="ca-pub-4969077794908710"
									data-ad-slot="6460499125"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                        </div>

				        <div class="social-share-btn d-flex align-items-center flex-wrap">
							SHARE :
							<a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a>
							<a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a>
							<a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="fa fa-instagram"></i></a>
							<a class="btn-whatsapp" href="whttps://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a>
							<a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a>
						</div>

						<div class="post-footer">
							<div class="gap-30"></div>
							<div class="tag-lists">
								<span>Tags: </span>
                                @if(isset($content['tag']) AND empty($content['tag'][0]['term_id']))
									@foreach($content['tag'] as $tag)
									@php
										$tag_name = $tag;
										$tag_slug = str_replace(' ', '-',$tag)
									@endphp
										<a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
									@endforeach
								@else
									@foreach($content['tag'] as $tag)
										<a href="{{ url("/tag/{$tag['slug']}") }}">{{ $tag['name'] }}</a>
									@endforeach
        						@endif
							</div><!-- tag lists -->

						</div>

					</div><!-- single-post end -->

					<!-- ads under article -->
					@include('includes.ads.under-article')

					<div class="gap-30 d-none d-md-block"></div>

					<div>
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terkini </span>
						</h2>
						<div class="row ts-gutter-20 loadmore-frame">
				          @php $no = 0; @endphp
				          @foreach($breakingcat as $posts)
				          @php $no++ @endphp
							<div class="col-12 mb-10 content-box">
								<div class="post-block-style">
									<div class="row">
										<div class="col-md-5">
											<div class="post-thumb post-list_feed">
												<img src="{{ $posts['images']['thumbnail'] }}" alt="{{ $posts['title'] }}" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
												<a class="post-cat-box {{ $posts['category'] }}" href="https://www.solopos.com/{{ $posts['category'] }}">{{ $posts['category'] }}</a>
											</div>
										</div>
										<div class="col-md-7 pl-0">
											<div class="post-content">
												<h2 class="post-title title-md">
												<a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" title="{{ $posts['title'] }}">{{ $posts['title'] }}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($posts['author']) {!! $posts['author'] !!} @endif</a></span>
													<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($posts['date']) }}</span>
												</div>
												<p>@if($posts['summary']) {{ $posts['summary'] }} @endif</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						  @php $no++; @endphp @endforeach
							<div class="col-12 mt-3 align-items-center" style="text-align: center;">
					            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Cek Berita Lainnya</a>
					            <a href="https://m.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
							</div><!-- col end -->
						</div>
					</div><!-- Content Col end -->
				</div><!-- col-lg-8 -->

				<!-- sidebar start -->
				@include('includes.sidebar-uksw')
				<!-- sidebar end -->
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->

	{{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
    @push('custom-scripts')
    {{-- <script>
      $(window).load(function() {
        $.ajax({
            type: "GET",
            url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
        });
    });
    </script> --}}
    @endpush
    @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush

@endsection
