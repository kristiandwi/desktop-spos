@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li><a href="{{ url("") }}"><i class="fa fa-home"></i></a></li>
						<li><i class="fa fa-angle-right" style="padding-right: 10px;"></i> Halaman Pencarian</li>
                        <li><i class="fa fa-angle-right"></i>{{ucwords($s)}}</li>
					</ol>		
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->

    <!-- Section Terkini Start -->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<div class="d-none d-md-block"></div>
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="xs-search-panel">									
                                    <form action="{{ route('search') }}" method="post" class="ts-search-group">
                                        <div class="input-group">
                                            {{ csrf_field() }}
                                            <input type="search" class="form-control" name="s" placeholder="Apa yang ingin ada cari ?" value="">
                                            <button class="input-group-btn search-button">
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
	
					<div class="gap-30 d-none d-md-block"></div>

					<div class="row ts-gutter-20 loadmore-frame">
			          @php $no = 1; @endphp
			          @foreach ($breaking as $item)
						@php           
						$image = $item['thumbnail'] ?? '{{ url("/images/no-thumb.jpg") }}'; 
						$title = html_entity_decode($item['title']);
						@endphp 
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $image }}" alt="{{ $title }}" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
											<a class="post-cat-box {{ $item['catsname'] }}" href="">{{ $item['catsname'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											@if($no==1)
											<h1 class="post-title title-md">
												{{-- @if($item['is_premium'] == 'premium') 
												<span class="espos-plus">+ PLUS</span>
												@endif --}}
												<a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=terkini_desktop" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
											</h1>											
											@else 
											<h2 class="post-title title-md">
												{{-- @if($item['is_premium'] == 'premium') 
												<span class="espos-plus">+ PLUS</span>
												@endif --}}
												<a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=terkini_desktop" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
											</h2>											
											@endif
											<div class="post-meta mb-7">
												{{-- <span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($item['author']) {!! $item['author'] !!} @endif</a></span> --}}
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Carbon\Carbon::parse($item['date'])->translatedFormat('l, j F Y H:i') }} WIB</span>
											</div>
											<p>@if($item['summary']) {!! $item['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                        @php $no++; @endphp
                        @endforeach	
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
				            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>									
				</div><!-- Content Col end -->

				<!-- sidebar home -->
				@include('includes.sidebar')

				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section>
    <!-- Section Terkini End -->

@endsection