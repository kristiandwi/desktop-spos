@extends('regional.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
    <section class="featured-post-area no-padding">
        <div class="container pl-0 pr-0">
            <div class="row ts-gutter-20">
                <div class="col-lg-8 col-md-12 pad-r">
                    <div class="owl-carousel owl-theme featured-slider h2-feature-slider" style="background-image:url({{ url('images/background-espos-plus.jpg') }});height:100%;border-radius:7px;">
                    @php $hl_loop = 1; @endphp
                    @foreach($headline as $hl)
                        @if($hl['category'] == $category)
                        @if($hl_loop <=5)
                        <div class="item post-overaly-style post-md" style="background-image:url({{ $hl['images']['thumbnail'] }})">
                            <div class="featured-post">
                                <a class="image-link" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">&nbsp;</a>
                                <div class="overlay-post-content">
                                    <div class="post-content">
                                        <div class="grid-category">
                                            <a class="post-cat {{ $hl['category'] }}" href="{{ url("/{$hl['category']}") }}">{{ $hl['category'] }}</a>
                                        </div>
										{{-- url('images/plus.png') --}}
                                        <h2 class="post-title title-md">
											@if($hl['konten_premium'] == 'premium')
											<span class="espos-plus">+ PLUS</span>
											@endif
											<a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
                                        </h2>
                                        <div class="post-meta">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-user"></i> @if($hl['author']) {!! $hl['author'] !!} @endif</a></li>
                                                <li><a href="#"><i class="icon icon-clock"></i> {{ Helper::time_ago($hl['date']) }}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!--/ Featured post end -->
                        </div><!-- Item 1 end -->

                    @endif @php $hl_loop++; @endphp @endif @endforeach
                    </div>
                </div><!-- Col 8 end -->
                @php $pc_loop = 1; @endphp
                @foreach($premium as $pc)
                @if($pc_loop == 5)
                <div class="col-lg-4 col-md-12">
                    <div class="post-overaly-style post-md" style="background-image:url({{ $pc['images']['thumbnail'] }})">
                        <div class="featured-post">
                            <a class="image-link" href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}" title="{{ html_entity_decode($pc['title']) }}">&nbsp;</a>
                            <div class="overlay-post-content">
                                <div class="post-content">
                                    <div class="grid-category">
                                    <a class="post-cat premium" href="https://www.solopos.com/plus">Espos Plus</a>
                                    </div>

                                    <h2 class="post-title title-md">
										<span class="espos-plus">+ PLUS</span>
                                        <a href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
                                    </h2>
                                    <div class="post-meta">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></li>
                                            <li><a href="#"><i class="icon icon-clock"></i>{{ Helper::time_ago($pc['date']) }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!--/ Featured post end -->
                    </div><!-- Item 2 end -->
                </div><!-- Col 4 end -->
                @endif @php $pc_loop++; @endphp @endforeach


            </div><!-- Row end -->
        </div><!-- Container end -->
    </section><!-- Feature post end -->
    <div class="gap-20"></div>
    <!-- ads top leaderboard -->

    <!-- Section Trending start-->
	<section class="trending-slider pb-0">
		<div class="container pl-0 pr-0">
			<div class="ts-grid-box">
				<h2 class="block-title">
					 <span class="title-angle-shap"> Fokus </span>
				</h2>
				<div class="owl-carousel dot-style2" id="trending-slider">
					@php $ec_loop = 1; @endphp
		          	@foreach($editorchoice as $ec)
                        @if($ec['category'] == $category)
                        @if($ec_loop <= 5)
					<div class="item post-overaly-style post-md" style="background-image:url({{ $ec['images']['thumbnail'] }})">
						<a class="image-link" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}?utm_source=editors_choice_desktop" title="{{ html_entity_decode($ec['title']) }}">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat {{ $ec['category'] }}" href="{{ url("/{$ec['category']}") }}">{{ $ec['category'] }}</a>
								</div>

								<h2 class="post-title">
									@if($ec['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif
									<a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}?utm_source=editors_choice_desktop" title="{{ html_entity_decode($ec['title'])  }}">{{ html_entity_decode($ec['title']) }}</a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>@if($ec['author']) {!! $ec['author'] !!} @endif</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
                    @endif @php $ec_loop++; @endphp @endif @endforeach
				</div>
				<!-- most-populers end-->
			</div>
			<!-- ts-populer-post-box end-->
		</div>
		<!-- container end-->
	</section>
	<!-- section trending End -->

    <!-- Section Terkini Start -->
	<section class="block-wrapper">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">
					<h2 class="block-title">
						<span class="title-angle-shap"> Berita Terkini </span>
					</h2>
					<div class="row ts-gutter-20 align-items-center">
			          @php $no = 1; @endphp
			          @foreach($breakingcat as $post) @if($no <= 3 )

			          @if($no==3)
			          <!-- ads advertorial -->
			          @endif
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $post['images']['url_thumb'] }}" alt="{{ html_entity_decode($post['title']) }}" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
											<a class="post-cat-box {{ $post['category'] }}" href="{{ url("/{$post['category']}") }}">{{ $post['category'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											@if($no==1)
											<h1 class="post-title title-md">
												@if($post['konten_premium'] == 'premium')
												<span class="espos-plus">+ PLUS</span>
												@endif
												<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title'])}}">{{ html_entity_decode($post['title']) }}</a>
											</h1>
											@else
											<h2 class="post-title title-md">
												@if($post['konten_premium'] == 'premium')
												<span class="espos-plus">+ PLUS</span>
												@endif
												<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h2>
											@endif
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($post['author']) {!! $post['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
											</div>
											<p>@if($post['summary']) {!! $post['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					  @endif @php $no++; @endphp @endforeach
					</div>

					<!-- Block Konten Premium -->
					<div class="block style2 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> Espos Plus</span>
						</h2>

						<div class="row">
				            @php $pc_loop = 1; @endphp
				            @foreach($premium as $pc) @if($pc_loop <= 5)

				            @if($pc_loop == 1)
							<div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}?utm_source=premium_desktop" title="{{ html_entity_decode($pc['title']) }}">
											<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ html_entity_decode($pc['title']) }}" style="object-fit: cover; width: 266px; height: 178px;">
										</a>
										<div class="grid-cat">
											<a class="post-cat premium" href="https://www.solopos.com/plus">Espos Plus</a>
										</div>
									</div>

									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											<span class="espos-plus">+ PLUS</span>
											<a href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}?utm_source=terkini_desktop" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
										</h2>
										<p>@if($pc['summary']) {!! $pc['summary'] !!} @endif</p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($pc['date']) }}</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">
							@endif

							@if( $pc_loop > 1 && $pc_loop <= 5 )

									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}?utm_source=terkini_desktop" title="{{ html_entity_decode($pc['title']) }}">
													<img src="{{ $pc['images']['url_thumb'] }}" alt="{{ html_entity_decode($pc['title']) }}" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>

											<div class="post-content">
												<h2 class="post-title mb-2">
													<span class="espos-plus">+ PLUS</span>
													<a href="https://www.solopos.com/{{ $pc['slug']}}-{{$pc['id']}}?utm_source=terkini_desktop" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->

							@endif
							@if($pc_loop == 5)
								</div><!-- .row -->
							</div><!-- Col 2 end -->
						@endif
                        @endif
                        @php $pc_loop++; @endphp
                    @endforeach
						</div><!-- Row end -->
					</div><!-- Block Konten Premium end -->

					<div class="row ts-gutter-20 loadmore-frame">
			          @php $no = 1; @endphp
			          @foreach($breakingcat as $post) @if($no > 3)
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ $post['images']['url_thumb'] }}" alt="{{ $post['title'] }}" style="object-fit: cover; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
											<a class="post-cat-box {{ $post['category'] }}" href="{{ url("/{$post['category']}") }}">{{ $post['category'] }}</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
												@if($post['konten_premium'] == 'premium')
												<span class="espos-plus">+ PLUS</span>
												@endif
												<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($post['author']) {!! $post['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
											</div>
											<p>@if($post['summary']) {!! $post['summary'] !!} @endif</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
                        @php $no++; @endphp
                        @endforeach
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
				            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>
				</div><!-- Content Col end -->

				<!-- sidebar home -->
				@include('regional.part.sidebar')

				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section>
    <!-- Section Terkini End -->

    <!-- ad banner start-->
	<div class="block-wrapper no-padding">
		<div class="container pl-0 pr-0">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="banner-img">
						<!-- ads leaderboard 2 -->
					</div>
				</div>
				<!-- col end -->
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

    <!-- ad banner start-->
	<div class="block-wrapper no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="banner-img text-center">
						<!-- ads leaderboard 3 -->
					</div>
				</div>
				<!-- col end -->
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

@endsection
