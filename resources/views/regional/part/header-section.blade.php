	@if(!empty($is_bob) AND $is_bob == 'yes')
	@include('includes.tematik.header-bob')

	@elseif(!empty($is_uksw) AND $is_uksw == 'yes')
	@include('includes.tematik.header-uksw')

	@elseif(!empty($is_bugar) AND $is_bugar == 'yes')
	@include('includes.tematik.header-bugar')

	@elseif(!empty($is_ubah) AND $is_ubah == 'yes')
	@include('includes.tematik.header-ubahlaku')

	@else
	@include('includes.ads.wrapt-top')
	<!-- Header start -->
	<header id="header" class="header">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-md-3 col-sm-12">
					<div class="header-nav">
						<a href="https://www.solopos.com/espospedia">Espospedia</a> | <a href="https://interaktif.solopos.com/" target="_blank"> Interaktif</a>

						<div class="top-social">
							<ul class="social list-unstyled" style="padding:0;">
								<li><a href="https://www.facebook.com/soloposcom" target="_blank" title="Facebook FP"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://www.twitter.com/soloposdotcom" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.instagram.com/koransolopos" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
								<li><a href="mailto:redaksi@solopos.co.id" target="_blank" title="Email"><i class="fa fa-envelope"></i></a></li>
							</ul>
						</div>

					</div>
				</div><!-- logo col end -->

				<div class="col-md-6 col-sm-12">
                    <div class="logo">

						@if(!empty($if_regional))
						<a href="{{ url('/') }}/jateng/{{ $regional_name }}">
						  <img class="logo-image" src="{{ url('images/logo-') }}{{ $regional_name }}.png" style="width:300px;" alt="Logo Regional">
						</a>
						@elseif(!empty($premium_content) AND $premium_content == 'premium')
						<a href="{{ url('/plus') }}">
						  <img class="logo-image" src="{{ url('images/plus.png') }}" style="width:250px;" alt="Espos Plus">
						</a>
						@else
						<a href="{{ url('/') }}">
						  <img class="logo-image" src="{{ url('images/logo-sekolah-solopos.png') }}" style="width:200px;" alt="Solopos.com">
						</a>
						@endif

                    </div>
				</div><!-- logo col end -->
				<div class="col-md-3 col-sm-12">
					<div class="header-nav" style="text-align: right;">
						<div style="height: 20px;display:inline;float: right; margin-bottom:10px;">
							<a href="https://www.solopos.com/plus">
								<img class="logo-image" src="{{ url('images/plus-small.png') }}" height="22" alt="Espos Plus">
							</a>
						</div>
						<div style="clear: right;font-weight:500;font-size:13px;">
						@if(Cookie::get('is_login') == 'true')
						<a href="https://id.solopos.com/profile" target="_blank">PROFIL</a> | <a href="https://id.solopos.com/subscribe" target="_blank">MEMBER</a>
						@else
						<a href="https://id.solopos.com/login" target="_blank">LOGIN</a> | <a href="https://id.solopos.com/register" target="_blank">DAFTAR</a>
						@endif
						</div>
					</div>
				</div><!-- logo col end -->

			</div><!-- Row end -->
		</div><!-- Logo and banner area end -->
	</header><!--/ Header end -->

	<div class="main-nav clearfix is-ts-sticky">
		<div class="container">
			<div class="row justify-content-between">
				<nav class="navbar navbar-expand-lg col-lg-12">
					<div class="site-nav-inner float-left">
					   	<!-- End of Navbar toggler -->
					   	<div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav">
								<li class="sticky">

			                        {{-- <img src="/images/logo-.png" alt="Logo"> --}}

									<img src="{{ url('/images/logo.png') }}" alt="Logo">

								</li>
								<li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
								<li><a href="{{ url('/') }}">Beranda</a></li>
                                <li><a href="{{ url('/urita') }}">Urita</a></li>
                                <li><a href="{{ url('/edukasi') }}">Edukasi</a></li>
                                <li><a href="{{ url('/mimbar-guru') }}">Mimbar Guru</a></li>
                                <li><a href="https://www.solopos.com/arsip">Index Berita</a></li>

								<!-- Features menu end -->

							</ul><!--/ Nav ul end -->
						</div><!--/ Collapse end -->
					</div><!-- Site Navbar inner end -->
					<div class="text-right" style="min-width: 65px; ">
						<ul class="nav navbar-nav" style="float: left;">
							<li class="dropdown mega-dropdown">
								<a style="font-size:18px;" href="#" class="dropdown-toggle menu-dropdown" data-toggle="dropdown"><i class="icon icon-menu"></i></a>
								<ul class="dropdown-menu" role="menu">
								<!-- responsive dropdown end -->
								<div class="ts-footer">
									<div class="container">
										<div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
											<div class="col-lg-7 col-md-7">
												<div class="footer-widget">
													<div class="footer-logo">
														<img src="{{ url('/images/logo.png') }}" alt="Footer Logo">
													</div>
													<div class="widget-content">
														<ul class="ts-footer-nav">
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/kontak">KONTAK</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/copyright">COPYRIGHT</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/foto">BERITA FOTO</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/about-us">REDAKSI</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/about-us">TENTANG KAMI</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/video">BERITA VIDEO</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/loker">KARIR</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/kode-etik">KODE ETIK</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/cekfakta">CEK FAKTA</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/privacy-policy">PRIVACY POLICY</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/page/code-of-conduct">PEDOMAN MEDIA SIBER</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.solopos.com/jagad-jawa">JAGAD JAWA</a></li>
														</ul>
													</div>
												</div>
											</div><!-- col end -->
											<div class="col-lg-5 col-md-5">
												<div class="footer-widtet post-widget">
													<h3 class="widget-title"><span>Jaringan</span></h3>
													<div class="widget-content">

														<ul class="ts-footer-nav">
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.bisnis.com" title="Bisnis.com" target="_blank" rel="noopener noreferrer">BISNIS.COM</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.harianjogja.com" title="Harianjogja.com" target="_blank" rel="noopener noreferrer">HARIANJOGJA.COM</a></li>
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://bisnismuda.id" title="Bisnismuda.id" target="_blank" rel="noopener noreferrer">BISNISMUDA.ID</a></li>
															{{--<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.iklan.solopos.com" title="Iklan Solopos" target="_blank" rel="noopener noreferrer">IKLAN</a></li>--}}
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.tokosolopos.com" title="Tokosolopos.com" target="_blank" rel="noopener noreferrer">TOKO</a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.semarangpos.com" title="Semarangpos.com" target="_blank" rel="noopener noreferrer">SEMARANGPOS.COM</a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.griya190.com" title="Griya190.com" target="_blank" rel="noopener noreferrer">GRIYA190.COM</a></li>

															{{--<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.jeda.id" title="Jeda.id" target="_blank" rel="noopener noreferrer">JEDA.ID</a></li>--}}
															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://pisalin.com/" title="Pisalin" target="_blank" rel="noopener noreferrer">PISALIN</a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.madiunpos.com" title="Madiunpos.com" target="_blank" rel="noopener noreferrer">MADIUNPOS.COM</a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.rumah190.com" title="Rumah190.com" target="_blank" rel="noopener noreferrer">RUMAH190.COM</a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#" title="" target="_blank" rel="noopener noreferrer"></a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#" title="" target="_blank" rel="noopener noreferrer"></a></li>

															<li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://www.ibukotakita.com" title="Ibukotakita.com" target="_blank" rel="noopener noreferrer">IBUKOTAKITA.COM</a></li>
														</ul>

													</div>
												</div>
											</div><!-- col end -->
										</div><!-- row end -->
									</div><!-- container end -->
								</div>
								</ul><!-- End dropdown -->
							</li><!-- Features menu end -->
						</ul><!--/ Nav ul end -->
						<div class="nav-search">
							<a href="#search-popup" class="xs-modal-popup">
								<i class="icon icon-search1"></i>
							</a>
						</div>
						<div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="xs-search-panel">
										<form action="{{ route('search') }}" method="post" class="ts-search-group">
											<div class="input-group">
												{{ csrf_field() }}
												<input type="search" class="form-control" name="s" placeholder="Search" value="">
												<button class="input-group-btn search-button">
													<i class="icon icon-search1"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</nav><!--/ Navigation end -->

			</div><!--/ Row end -->
		</div><!--/ Container end -->
		<div class="trending-bar trending-light d-md-block">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-9 text-center text-md-left">
						<p class="trending-title"><i class="tsicon fa fa-bolt"></i> STORY </p>
						<div id="trending-slide" class="owl-carousel owl-theme trending-slide">
							@php $sto_loop = 1; @endphp
							@foreach($story as $sto) @if($sto_loop <= 5)
							<div class="item">
							   <div class="post-content">
							      <h2 class="post-title title-small">
							         <a href="{{ url("/{$sto['slug']}-{$sto['id']}") }}" target="_blank" title="{{ $sto['title'] }}">{{ $sto['title'] }}</a>
							      </h2>
							   </div>
							</div>
					      	@endif
							@php $sto_loop++; @endphp
							@endforeach
						</div>
					</div><!-- Col end -->
					<div class="col-md-3 text-md-right text-center">
						<div class="ts-date">
							<i class="fa fa-calendar-check-o"></i>{{ date("j F Y") }}
						</div>
					</div><!-- Col end -->
				</div><!--/ Row end -->
			</div><!--/ Container end -->
		</div><!--/ Trending end -->
	</div><!-- Menu wrapper end -->
	<div class="gap-50"></div>
	@include('includes.ads.live-leaderboard')
	<div class="gap-20"></div>
    <!-- ads floating -->

    <!-- ads top leaderboar -->
	@endif
